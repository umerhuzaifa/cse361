% On plotting the saved data and analyzing for finding the first order
% system parameters


% Umer Huzaifa
% 7/14/2021

clear all
close all

%%%%%%%%% Reading the Arduino CSV file Now %%%%%%%%%%%%%%

subplot(211)

T = readtable('motor_data_ard.csv');

t_ard = T{:, 1};  % note that the time stamps start from 0.225 s instead of 0 s
v_ard = T{:, 2};


plot(t_ard, v_ard)

title('Step Response of a Motor', 'interpreter', 'latex')
xlabel('Time (sec)', 'interpreter', 'latex')
ylabel('Angular Velocity (rad/s)', 'interpreter', 'latex')

set(gca,'FontSize', 25,'TickLabelInterpreter','latex')
 
set(gcf, 'Color', 'white')



%%%%%%%%% Reading the RPI CSV file Now %%%%%%%%%%%%%%


subplot(212)

T= readtable('motor_data_pi.csv');

if (size(T, 2)==2)
    t_pi = T{:, 1};  % note that the time stamps start from 0.225 s instead of 0 s
    v_pi = T{:, 2};
else
    t_pi = T{:, 2};  % note that the time stamps start from 0.225 s instead of 0 s
    del_pi = T{:, 1};
    v_pi = T{:, 3};
end

plot(t_pi, v_pi)

title('Step Response of a Motor', 'interpreter', 'latex')
xlabel('Time (sec)', 'interpreter', 'latex')
ylabel('Angular Velocity (rad/s)', 'interpreter', 'latex')

set(gca,'FontSize', 25,'TickLabelInterpreter','latex')
 
set(gcf, 'Color', 'white')



%%%%%%%%%%%%%% Using the cftool now %%%%%%%%%%%%%%%%%

t_step = t_pi(381:450);
v_step = v_pi(381:450);
model.t = t_step;
model.v = v_step;

figure
plot(t_step, v_step)
title('Extracted Data for Step Analaysis of the Motor')

save('8_5_Speed12V.mat','model')


%%
%%%%%%%%%%%%% Symbolic Calculations %%%%%%%%%%%%%%%%%

syms K tau s real

frac = (s*K/12*tau)/(s+1/tau);

pretty(ilaplace(frac));
