% On plotting the saved data and analyzing for finding the first order
% system parameters


% Umer Huzaifa
% 7/14/2021

% Arduino Step Data

% 1/2/2022

clear all
close all

%%%%%%%%% Reading the Arduino CSV file Now %%%%%%%%%%%%%%

subplot(211)

T = readtable('motor_data_ard.csv');

t_ard = T{:, 1};  % note that the time stamps start from 0.225 s instead of 0 s
v_ard = T{:, 2}*550/990;


plot(t_ard, v_ard)

title('Step Response of a Motor from Arduino', 'interpreter', 'latex')
xlabel('Time (sec)', 'interpreter', 'latex')
ylabel('Angular Velocity (rad/s)', 'interpreter', 'latex')

set(gca,'FontSize', 25,'TickLabelInterpreter','latex')
 
set(gcf, 'Color', 'white')



%%%%%%%%% Reading the RPI CSV file Now %%%%%%%%%%%%%%


subplot(212)

T= readtable('motor_data_pi.csv');

if (size(T, 2)==2)
    t_pi = T{:, 1};  % note that the time stamps start from 0.225 s instead of 0 s
    v_pi = T{:, 2};
else
    t_pi = T{:, 2};  % note that the time stamps start from 0.225 s instead of 0 s
    del_pi = T{:, 1};
    v_pi = T{:, 3};
end

plot(t_pi, v_pi)

title('Step Response of a Motor from RPi', 'interpreter', 'latex')
xlabel('Time (sec)', 'interpreter', 'latex')
ylabel('Angular Velocity (rad/s)', 'interpreter', 'latex')

set(gca,'FontSize', 25,'TickLabelInterpreter','latex')
 
set(gcf, 'Color', 'white')




%% For the data from Tera Term Arduino 
% 1/2/2022

clear all
close all
clc


ard_csv = readtable('teraterm.csv');
time = ard_csv{:, 1};
speed = ard_csv{:, 2};
% speed_f = lowpass(speed, 0.05);
movAvg = dsp.MovingAverage(2);
speed_f = movAvg(speed);
plot(time,speed)
hold on
plot(time, speed_f)

% CFTOOL gave the following parameters on raw data

a = 10.95;
b = 23.36;

% using the equation; a*(1-exp(-b*x/1000000))

save('motor_model.mat', 'a', 'b', 'time','speed', 'speed_f');
