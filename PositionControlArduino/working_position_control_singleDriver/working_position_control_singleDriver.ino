// This alternate version of the code does not require
// atomic.h. Instead, interrupts() and noInterrupts() 
// are used. Please use this code if your 
// platform does not support ATOMIC_BLOCK.


// Updated: March 17, 2022
// Making the position controller code on Arduino to work for 
// a single pin motor driver circuit (Arduino Motor Shield, DF Robot Shield etc.)

#define ENCA 2 // YELLOW
#define ENCB 3 // WHITE
#define PWM 11

#define IN1 13

volatile int posi = 0; // specify posi as volatile: https://www.arduino.cc/reference/en/language/variables/variable-scope-qualifiers/volatile/
long prevT = 0;
float eprev = 0;
float eintegral = 0;

void setup() {
  Serial.begin(9600);
  pinMode(ENCA,INPUT);
  pinMode(ENCB,INPUT);
  attachInterrupt(digitalPinToInterrupt(ENCA),readEncoder,RISING);
  
  pinMode(PWM,OUTPUT);
  pinMode(IN1,OUTPUT);
  
  Serial.println("target pos");
}

void loop() {

 

  // PID constants
  float kp = 3;//10
  float kd = 0.15; //0.15
  float ki = 1;//4

  // time difference
  long currT = micros();
  float deltaT = ((float) (currT - prevT))/( 1.0e6 );
  prevT = currT;

  
 // set target position
  //int target = 1200;
//  int target = 90*sin(currT/1e6);

  float target = Serial.read();
  // Read the position
  float pos = 0; 
  noInterrupts(); // disable interrupts temporarily while reading
  pos = (float) posi/200 *360;  // the dividing factor is odd and not found in the datasheet but seems to work for us
  interrupts(); // turn interrupts back on
  
  // error
  int e = pos - target;

  // derivative
  float dedt = (e-eprev)/(deltaT);

  // integral
  eintegral = eintegral + e*deltaT;

  // control signal
  float u = kp*e + kd*dedt + ki*eintegral;

  // motor power
  float pwr = fabs(u);
  if( pwr > 255 ){
    pwr = 255;
  }

//  else if (pwr<70){
//
//    pwr = 70;  // minimum value to 
//  }

  // motor direction
  int dir = 1;
  if(u<0){
    dir = -1;
  }

  // signal the motor
  setMotor(dir,pwr,PWM,IN1);


  // store previous error
  eprev = e;

  Serial.print(target);
  Serial.print(" ");
  Serial.print(pos);
  Serial.println();
}

void setMotor(int dir, int pwmVal, int pwm, int in1){
  analogWrite(pwm,pwmVal);
  if(dir == 1){
    digitalWrite(in1,HIGH);
    
  }
  else if(dir == -1){
    digitalWrite(in1,LOW);
    
  }
  
}

void readEncoder(){
  int b = digitalRead(ENCB);
  if(b > 0){
    posi++;
  }
  else{
    posi--;
  }
}
