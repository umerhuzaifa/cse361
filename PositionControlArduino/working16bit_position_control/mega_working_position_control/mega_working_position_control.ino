// This alternate version of the code does not require
// atomic.h. Instead, interrupts() and noInterrupts() 
// are used. Please use this code if your 
// platform does not support ATOMIC_BLOCK.

#define ENCA 20 // YELLOW
#define ENCB 3 // WHITE
#define PWM 11
#define IN2 6
#define IN1 7

volatile int posi = 0; // specify posi as volatile: https://www.arduino.cc/reference/en/language/variables/variable-scope-qualifiers/volatile/
long prevT = 0;
float eprev = 0;
float eintegral = 0;
uint16_t icr         = 65535; //32767;//0xffff;

void setup() {
  setupPWM16();
  Serial.begin(9600);
  pinMode(ENCA,INPUT);
  pinMode(ENCB,INPUT);
  attachInterrupt(digitalPinToInterrupt(ENCA),readEncoder,RISING);
  
  pinMode(PWM,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  
  Serial.println("target pos");
}

void setupPWM16()
{

  ///////////////////////
    DDRB |= _BV(PB1) | _BV(PB2); //Set pins as outputs 
    TCCR1A = _BV(COM1A1) | _BV(COM1B1) //Non-Inv PWM 
    | _BV(WGM11); // Mode 14: Fast PWM, TOP=ICR1
    TCCR1B = _BV(WGM13) | _BV(WGM12)
    | _BV(CS10); // Prescaler 1
    ICR1 = icr; // TOP counter value (Relieving OCR1A*)
}
void loop() {

 

  // PID constants
  float kp = 500;  //1000;
  float kd = 50;  //100;
  float ki = 0;  //800;

  // time difference
  long currT = micros();
  float deltaT = ((float) (currT - prevT))/( 1.0e6 );
  prevT = currT;

  
 // set target position
  //int target = 1200;
  int target = 120*sin(currT/2/1e6);

  // Read the position
  float pos = 0; 
  noInterrupts(); // disable interrupts temporarily while reading
  pos = (float) posi/550 *360;
  interrupts(); // turn interrupts back on
  
  // error
  int e = pos - target;

  // derivative
  float dedt = (e-eprev)/(deltaT);

  // integral
  eintegral = eintegral + e*deltaT;

  // control signal
  float u = kp*e + kd*dedt + ki*eintegral;

  // motor power
  float pwr = fabs(u);
  if( pwr > 65535 ){
    pwr = 65535;
  }

  // motor direction
  int dir = -1;
  if(u<0){
    dir = 1;
  }

  // signal the motor
  setMotor(dir,pwr,PWM,IN1,IN2);


  // store previous error
  eprev = e;
  
  Serial.print(dir);
  Serial.print(" ");
  Serial.print(target);
  Serial.print(" ");
  Serial.print(pos);
  Serial.print(" ");
  Serial.print(pwr);
  Serial.println();
}

void setMotor(int dir, int pwmVal, int pwm, int in1, int in2){
  analogWrite(pwm,pwmVal);
  if(dir == 1){
    digitalWrite(in1,HIGH);
    digitalWrite(in2,LOW);
  }
  else if(dir == -1){
    digitalWrite(in1,LOW);
    digitalWrite(in2,HIGH);
  }
  else{
    digitalWrite(in1,LOW);
    digitalWrite(in2,LOW);
  }  
}

void readEncoder(){
  int b = digitalRead(ENCB);
  if(b > 0){
    posi--;
  }
  else{
    posi++;
  }
}
