# Code for reading the serial data from Motor and displaying in real time

# Works perfectly:

##- Plots real time speed against time sample
##- Stores the time samples and the speed data in a separate file
##
#!/usr/bin/env python

from sys import platform
from threading import Thread
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct

import csv

port_mac = '/dev/cu.usbserial-DA017NVX'
port_pc  = 'COM6'
class serialPlot:
    def __init__(self, serialPort, serialBaud = 115200, plotLength = 100, dataNumBytes = 3):
        self.port = serialPort
        self.baud = serialBaud
        self.plotMaxLength = plotLength
        self.data = []
        self.tseries = []
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        self.plotTimer = 0
        self.previousTimer = 0

        print('Trying to connect to: ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        try:
            self.serialConnection = serial.Serial(serialPort, serialBaud, timeout=4)
            print('Connected to ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        except:
            print("Failed to connect with " + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')

    def readSerialStart(self):
        if self.thread == None:
            self.thread = Thread(target=self.backgroundThread)
            self.thread.start()
            while self.isReceiving != True:
                time.sleep(0.1)

    def getSerialData(self, frame, axes, lines, lineValueText, lineLabel, timeText):
        currentTimer = time.perf_counter()
        self.plotTimer = int((currentTimer - self.previousTimer))     # the first reading will be erroneous
        self.previousTimer = currentTimer
        timeText.set_text('Plot Interval = ' + str(self.plotTimer) + 'ms')
        strdata = self.rawData.decode('UTF-8')       # converting the byte literal to string literal
        
        strdata = strdata[0:-2]   # Removing carriage return and newline escape characters from the string
##        print(strdata)
        
        strlist = strdata.split(',')
        if not strlist[0]=='':
            tstamp = float(strlist[0])/1e6
            value = float(strlist[1])        
            self.data.append(value)    # we get the latest data point and append it to our array
            self.tseries.append(tstamp)
        
            # updating the plots using the updated data and timeseries received

            lines.set_data(self.tseries,self.data)
            axes.set_xlim([0, self.tseries[-1]])
            axes.set_ylim([min(self.data), max(self.data)])
            
            lineValueText.set_text('[' + lineLabel + '] = ' + str(value))
        else:
            self.isRun=False

    def backgroundThread(self):    # retrieve data
        self.serialConnection.reset_input_buffer()
        while (self.isRun):
            self.rawData = self.serialConnection.readline()
            self.isReceiving = True

    def close(self):
        self.isRun = False
        self.thread.join()
        self.serialConnection.close()
        print('Disconnected...')
        
        with open('motor_data_ard.csv','w') as f:
            for i in range(len(self.tseries)):
                f.write("%2.4f, %2.4f \n" %(self.tseries[i], self.data[i]))
                
def main():
    if platform == 'darwin':
        serialPort = port_mac
    else:
        serialPort = port_pc 
    baudRate = 115200
    maxPlotLength = 1000
    dataNumBytes = 3      # number of bytes of 1 data point
    s = serialPlot(serialPort, baudRate, maxPlotLength, dataNumBytes)   # initializes all required variables
    s.readSerialStart()                                               # starts background thread

    # plotting starts below
    pltInterval = 50    # Period at which the plot animation updates [ms]
    xmin = 0
    xmax = maxPlotLength
    ymin = -6
    ymax = 6
    fig = plt.figure()
    ax = plt.axes(xlim=(xmin, xmax), ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))
    ax.set_title('Motor Speed from Arduino')
    ax.set_xlabel("Time (sec.)")
    ax.set_ylabel("Motor Speed (rad/s)")
    axes = plt.gca()
    lineLabel = 'Measured Motor Speed (rad/s)'
    timeText = ax.text(0.50, 0.95, '', transform=ax.transAxes)
    lines = ax.plot([], [], label=lineLabel)[0]
    lineValueText = ax.text(0.50, 0.90, '', transform=ax.transAxes)
    anim = animation.FuncAnimation(fig, s.getSerialData, fargs=(axes, lines, lineValueText, lineLabel, timeText), interval=50)    # fargs has to be a tuple

    plt.legend(loc="upper left")
    plt.show()

    tseries = s.tseries
    data    = s.data
    print(len(tseries))
    s.close()

    plt.figure()
    plt.plot(tseries, data)
    plt.title('Step Response of the DC Motor Speed')
    plt.xlabel('Time (sec)')
    plt.ylabel('Speed (rad/s)')
    plt.show()
    
if __name__ == '__main__':
    main()
