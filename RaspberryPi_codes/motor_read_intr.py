# Purpose: Read and save the speed data from the DC motor
# Author: Umer Huzaifa
# Dated: 7/28/2021



import RPi.GPIO as GPIO
import time as t
#import matplotlib.pyplot as plt

rpm=0
radps=0


global counter
global init_time
global delta_time
global npulse

npulse = 100
counter =0
delta_time = 0
start_time = t.time()
init_time  = t.time()


def enc_callback_A(self):
    global counter
    global init_time
    global delta_time
    global npulse
    #A=GPIO.input(A_pin)
    #A = 1 # since has received a rising edge
    #B = GPIO.input(B_pin)
    #current_AB = (A<<1) | B
    #position = (last_AB << 2) | current_AB
    #counter+= outcome[position]
    #last_AB = current_AB
#     print('So we come here too')
    counter+=1
    if abs(counter - npulse)<=10:
        delta_time = t.time() - init_time
        counter = 0
        init_time = t.time()
    else:
        delta_time = 0 # indicating that radps measurement is incorrect right now
    
if __name__=='__main__':
    
    A_pin = 37
    B_pin = 38

    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(A_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    GPIO.add_event_detect(A_pin, GPIO.RISING, callback=enc_callback_A)

    
    f = open('../motor_data_pi.csv','w')
    ts = 0   # timestamp
    radps_filt_prev = 0
    alpha = 0.8
    
    while (ts <=20):
        ts = t.time() - start_time
        if (delta_time>0):   # valid data is available right now
            
            rpm = npulse/delta_time * 1/550 * 60  # rev per min = pulses per sec. * 1/ pulses per rev * sec. in a minute
            radps = rpm * 6.28/60   # radians per sec = rev. per min. * radians per min. * 1/sec. per min.
            radps = (1-alpha) * radps + alpha * radps_filt_prev
            radps_filt_prev = radps
            f.write("%2.4f, %2.4f \n"%(ts, radps))
            print(ts, delta_time, ' Motor Speed (rad/s):', radps)
            
        
    f.close()