#!/usr/bin/env python3

# make sure to install numpy and the corresponding matplotlib
# before going ahead with this.

import matplotlib #.pyplot as plt
matplotlib.rcParams['backend'] = 'cairo'
import numpy as np
import csv

filename="../motor_data_pi.csv"

t_pi  = []
v_pi = []
rows =[]

with open(filename, 'r') as csvfile:
	csvreader = csv.reader(csvfile)
	fields = next(csvreader)
	for row in csvreader:
		rows.append(row)
	print("Total no. of rows is : %d " %(csvreader.line_num))
#print('Field names are: ' + ' .join(field for field in fields))

#n = 100
#print('\n First %d rows are: \n'%n)

#for row in range(len(rows)):
#for col in row:
	
for i in range(len(rows)):
# 	print('%2.4f, %2.4f' %(float(rows[i][0]),float(rows[i][1])))
# 	print('\n')
	t_pi.append(float(rows[i][0]))
	v_pi.append(float(rows[i][1]))
    
for i in range(len(t_pi)):
	print('%2.4f , %2.4f' %(t_pi[i], v_pi[i]))
	
# matplotlib.pyplot.figure()
# matplotlib.pyplot.plot(t_pi, v_pi)
# matplotlib.pyplot.show()
    