# Purpose: Read and save the speed data from the DC motor
# Author: Umer Huzaifa
# Dated: 7/28/2021



import RPi.GPIO as GPIO
import time as t
#import matplotlib.pyplot as plt


# global counter
# global init_time
# global delta_time
# global npulse

npulse = 10
counter =0
delta_time = 0
start_time = t.time()
init_time  = start_time


def enc_callback_A(self):
    global counter
    global init_time
    global delta_time
    global npulse    
    counter+=1
    
    if abs(counter-npulse)<=1: 
        delta_time = t.time() - init_time
        counter = 0
        init_time = t.time() # updating the next reference time stamp
    else:
        delta_time = 0 # indicating that radps measurement is incorrect right now
    
if __name__=='__main__':
    
    A_pin = 37
    B_pin = 38
    
    rpm=0
    radps=0
    gear_ratio = 50 
    ppr = 11 # pulses per revolution
    T   = 5   # total time for the simulation

    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(A_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    GPIO.add_event_detect(A_pin, GPIO.RISING, callback=enc_callback_A)

    
    
    f = open('../motor_data_pi.csv','w')
    ts = 0
    radps_prev = 0
    while (True):
        ts = t.time() - start_time
        if (delta_time!=0):   # valid data is available right now            
            rpm = npulse/delta_time * 1/(ppr*gear_ratio) * 60  # rev per min = pulses per sec. * 1/ pulses per rev * sec. in a minute
            radps = rpm * 6.28/60   # radians per sec = rev. per min. * radians per min. * 1/sec. per min.
            radps = 0.8 * radps + 0.2 * radps_prev
            radps_prev = radps            
#         elif (t.time() - init_time<=1.5 * timeout):  # if not enough time has passed, go with the previous measure
#             radps = radps_prev
#         else:   # if enough time has passed and there is no delta_time update, declare the speed to be zero
#             radps =0
#             print(delta_time)
        f.write("%2.4f,%2.4f, %2.4f \n"%(delta_time,ts, radps))
        print(ts, ' Motor Speed (rad/s):', radps)
    f.close()
