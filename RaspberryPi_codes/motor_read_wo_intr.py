import RPi.GPIO as GPIO
import time as t
#import matplotlib.pyplot as plt

rpm=0
radps=0

delta_time = 0
start_time = t.time()
init_time  = t.time()


if __name__=='__main__':
    
    A_pin = 37
    B_pin = 38

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(A_pin, GPIO.IN,pull_up_down=GPIO.PUD_UP)
    GPIO.setup(B_pin, GPIO.IN,pull_up_down=GPIO.PUD_UP)

    counter=0
    npulse = 1e6
    
    try:
        while True:
            state_A = GPIO.input(A_pin)
            state_B = GPIO.input(B_pin)
            print(state_A, state_B)
    except KeyboardInterrupt:
        print('Shutting it down')
        GPIO.cleanup()
        