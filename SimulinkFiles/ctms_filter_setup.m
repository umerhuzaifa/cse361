clear all
close all
clc


Ts = 0.02;  % model sample time in seconds
filter_const = 5 * Ts; % Time constant for the first order filter
GR = 1/90; % gear ratio