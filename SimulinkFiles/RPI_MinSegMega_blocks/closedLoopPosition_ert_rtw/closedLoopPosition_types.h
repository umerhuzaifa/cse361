#include "__cf_closedLoopPosition.h"
#ifndef RTW_HEADER_closedLoopPosition_types_h_
#define RTW_HEADER_closedLoopPosition_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef struct_tag_04b6x6tDldj9IrIH2gmbjG
#define struct_tag_04b6x6tDldj9IrIH2gmbjG
struct tag_04b6x6tDldj9IrIH2gmbjG { int16_T __dummy ; } ;
#endif
#ifndef typedef_flqvy5y4vi
#define typedef_flqvy5y4vi
typedef struct tag_04b6x6tDldj9IrIH2gmbjG flqvy5y4vi ;
#endif
#ifndef struct_tag_AYWgtFscQm3mLUJYb3A20C
#define struct_tag_AYWgtFscQm3mLUJYb3A20C
struct tag_AYWgtFscQm3mLUJYb3A20C { boolean_T matlabCodegenIsDeleted ;
int32_T isInitialized ; boolean_T isSetupComplete ; flqvy5y4vi
DigitalIODriverObj ; } ;
#endif
#ifndef typedef_dizxgp3uub
#define typedef_dizxgp3uub
typedef struct tag_AYWgtFscQm3mLUJYb3A20C dizxgp3uub ;
#endif
#ifndef struct_tag_LZATNKaowXRlwzpKcQ6wRE
#define struct_tag_LZATNKaowXRlwzpKcQ6wRE
struct tag_LZATNKaowXRlwzpKcQ6wRE { boolean_T matlabCodegenIsDeleted ;
int32_T isInitialized ; boolean_T isSetupComplete ; } ;
#endif
#ifndef typedef_grejiqbszg
#define typedef_grejiqbszg
typedef struct tag_LZATNKaowXRlwzpKcQ6wRE grejiqbszg ;
#endif
#include "MW_SVD.h"
#ifndef struct_tag_g1WX34VPn8QzVKQriB4HN
#define struct_tag_g1WX34VPn8QzVKQriB4HN
struct tag_g1WX34VPn8QzVKQriB4HN { MW_Handle_Type MW_PWM_HANDLE ; } ;
#endif
#ifndef typedef_cthaup12iv
#define typedef_cthaup12iv
typedef struct tag_g1WX34VPn8QzVKQriB4HN cthaup12iv ;
#endif
#ifndef struct_tag_4eDVzK5FiryyCjvjjvrZGB
#define struct_tag_4eDVzK5FiryyCjvjjvrZGB
struct tag_4eDVzK5FiryyCjvjjvrZGB { boolean_T matlabCodegenIsDeleted ;
int32_T isInitialized ; boolean_T isSetupComplete ; cthaup12iv PWMDriverObj ;
} ;
#endif
#ifndef typedef_fyiasx4xee
#define typedef_fyiasx4xee
typedef struct tag_4eDVzK5FiryyCjvjjvrZGB fyiasx4xee ;
#endif
#ifndef struct_tag_E4MiKCePvAaKlBGgcN5h8E
#define struct_tag_E4MiKCePvAaKlBGgcN5h8E
struct tag_E4MiKCePvAaKlBGgcN5h8E { boolean_T matlabCodegenIsDeleted ;
int32_T isInitialized ; boolean_T isSetupComplete ; } ;
#endif
#ifndef typedef_mztvgr4sbz
#define typedef_mztvgr4sbz
typedef struct tag_E4MiKCePvAaKlBGgcN5h8E mztvgr4sbz ;
#endif
typedef struct ljjaqdure1k_ ljjaqdure1k ; typedef struct dorwdnxfv1
hetvy2qotg ;
#endif
