classdef Micros < realtime.internal.SourceSampleTime ...
        & coder.ExternalDependency ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.internal.CustomIcon
    %
    % Returns the number of microseconds since the Arduino board began running the current program
    % 
    %
    
    % Copyright 2019 The MathWorks, Inc.
    %#codegen
    
    properties
        % Public, tunable properties.
    end
    
    properties (Nontunable)
        % Public, non-tunable properties.
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    methods
        % Constructor
        function obj = Micros(varargin)
            coder.allowpcode('plain');
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)
        function setupImpl(obj) %#ok<MANU>
            if isempty(coder.target)
            else
                coder.cinclude('MW_Micros.h');
            end
        end
        
        function y = stepImpl(obj)   %#ok<MANU>
            y = uint32(0);
            if isempty(coder.target)
                % Place simulation output code here
            else
                % Call C-function implementing device output
                y = coder.ceval('mw_getMicros');
            end
        end
        
        function maskDisplayCmds = getMaskDisplayImpl(~)
            outport_label = '';
            maskDisplayCmds_Common = [...
                    ['color(''white'');',newline]...
                    ['plot([100,100,100,100]*1,[100,100,100,100]*1);',newline]...
                    ['plot([100,100,100,100]*0,[100,100,100,100]*0);',newline]...
                    ['color(''black'');',newline]...                                     % Drawing mask layout of the block
                    ['sppkgroot = strrep(codertarget.arduinobase.internal.getBaseSpPkgRootDir(),''\'',''/'');',newline]...
                    ['color(''blue'');',newline] ...
                    ['color(''black'');', newline] ...
                    ['text(50, 50, ''\fontsize{12}' '\bfMicros' ''',''texmode'',''on'',''horizontalAlignment'', ''center'');',newline]  ...
                ];
            
                maskDisplayCmds_Specific = outport_label;
            maskDisplayCmds = [maskDisplayCmds_Common maskDisplayCmds_Specific];
        end
    end
    
    methods (Access=protected)
        %% Define output properties
        function num = getNumInputsImpl(~)
            num = 0;
        end
        
        function num = getNumOutputsImpl(~)
            num = 1;
        end
        
        function flag = isOutputSizeLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isOutputFixedSizeImpl(~,~)
            varargout{1} = true;
        end
        
        function flag = isOutputComplexityLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isOutputComplexImpl(~)
            varargout{1} = false;
        end
        
        function varargout = getOutputSizeImpl(~)
            varargout{1} = [1,1];
        end
        
        function varargout = getOutputDataTypeImpl(~)
            varargout{1} = 'uint32';
        end
                
        function varargout = getOutputNamesImpl(~)
            varargout{1} = '';
        end
    end
    
    methods (Static, Access=protected)
        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end
        
        function isVisible = showSimulateUsingImpl
            isVisible = false;
        end
        
         function header = getHeaderImpl()
            header = matlab.system.display.Header(mfilename('class'),...
            'ShowSourceLink', false, ...
            'Title','Micros', ...
            'Text','Returns the number of microseconds since the Arduino board began running the current program');
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'Source';
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end
               
        
        function updateBuildInfo(buildInfo, context)           
             codertarget.arduinobase.internal.arduinoI2CWrite.updateBuildInfo(buildInfo, context);
             spkgrootDir = codertarget.arduinobase.internal.getBaseSpPkgRootDir;
             % Include Paths
             addIncludePaths(buildInfo, fullfile(spkgrootDir, 'include'));
             % Source Files
             systemTargetFile = get_param(buildInfo.ModelName,'SystemTargetFile');
             if isequal(systemTargetFile,'ert.tlc')
                 % Add the following when not in rapid-accel simulation
                 addSourcePaths(buildInfo, fullfile(spkgrootDir,'src'));
                 addSourceFiles(buildInfo,'MW_Micros.cpp', fullfile(spkgrootDir,'src'),'BlockModules');
             end
        end
    end
end
