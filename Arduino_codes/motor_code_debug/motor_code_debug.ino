/*


    Code for measuring the motor speed with fixed time period and couting the number of pulses in that time.

    Umer Huzaifa
    August 12, 2021

*/


/* Geared DC motor with Hall Effect Quadrature Encoder
   L298N Motor Driver

   Wiring:
   VCC -> 5V
   GND -> GND
   OUT A -> D2 (external interrupt)
   OUT B -> D3 (external interrupt)
   M1 -> Motor Driver A (Pin 11)
   M2 -> Motor Driver B (None)
   Motor Driver Input 1 -> 3
*/

#include<avr/interrupt.h>

#define encoder1 2
#define encoder2 3
#define MICRO_TO_SEC 1000000
#define FIX_TIME  5000

#define motorB 9 //5

#define motor_in1 6  // Active high operation
#define motor_in2 7  // Active high operation


/////////////////////////////////////////
/////////////////////////////////////////

volatile int count1 = 0;
volatile int count2 = 0;

uint8_t npulses = 0;   // pulses measured in a given amount of time

int meas_Dir = 0;  // 1 = CW
// 0 = Stationary
// -1 = CCW

// Timing the duration between two events:

long time_del = 0;                   // time variable for calculating the delay between two sets of pulses (variable defined above)
long new_time = 0;                   // time variable for the pulse arriving the second time
long init_time = micros();           // a reference for delay calculation
long start_time = micros();  //  the startup time, reference for the timestamp
long timestamp = 0;
float prev_timestamp = 0;


// speed variables


float rpm_meas = 0;                   // revolutions per minute speed
float radps_meas = 0;                 // radians per second speed
float radps_adjusted = 0;             // radians per second found by averaging with last measurement
float prev_filtered = 0;              // last measurement of radians per second
float acc_calc      = 0;

// Speed Data

float error     = 0;
float sum_error = 0;
float des_speed = 4.5;

uint16_t pwm         = 0;
uint16_t pwm_max     = 65535; // 65535;
uint16_t pwm_min     = 20000;


// Speed Control Variables

float pid_control = 0;
float maxcontroller = 1000;
float mincontroller = -1000;


float Kp = 4;
float Kd = 0; // 2.5;
float Ki = 0; //U 2.0;

float step1_start = 1500000;  // 1.5 s

// Low pass filter for the speed

float alpha = 0.8;

/////////////////////////////////////////
/////////////////////////////////////////

void brake()
{
  digitalWrite(motor_in1, LOW);
  digitalWrite(motor_in2, LOW);

}

void CW()
{


  digitalWrite(motor_in1, LOW);
  digitalWrite(motor_in2, HIGH);
}

void CCW()
{

  digitalWrite(motor_in1, HIGH);
  digitalWrite(motor_in2, LOW);
}

bool brake_check()
{
  if (digitalRead(motor_in1) && digitalRead(motor_in2) == true)
    return true;
  else
    return false;

}

void setup()
{

  setupPWM16();
  Serial.begin(115200);
  // Should it be a pull up input or a simple one??

  pinMode(encoder1, INPUT);
  pinMode(encoder2, INPUT);
  pinMode(motor_in1, OUTPUT);
  pinMode(motor_in2, OUTPUT);
  pinMode(motorB, OUTPUT);

  brake();  // starting with a brake

//   An initial movement given using the desired speed
  if (des_speed >= 0)
    CCW();
  else
    CW();

  // Initial values of all the speed control variables
  radps_meas = 0;
  prev_filtered = 0;  // seeing if the spike can be avoided by eliminating this reset
  radps_adjusted = 0;
  sum_error     = 0;
  error         = 0;
}

void setupPWM16()
{
    DDRB |= _BV(PB1) | _BV(PB2);        /* set pins as outputs */
    TCCR1A |= _BV(COM1A1) | _BV(COM1B1)  /* non-inverting PWM */
              | _BV(WGM11);                   /* mode 14: fast PWM, TOP=ICR1 */
    TCCR1B |= _BV(WGM13) | _BV(WGM12)
              | _BV(CS10);                    /* no prescaling */
    ICR1 = 0xffff;                      /* TOP counter value */


//  // Clear the control registers.
//  TCCR1A = 0;
//  TCCR1B = 0;
//  TCNT1  = 0;
//
//  // Mode 10: phase correct PWM with ICR1 as Top (= F_CPU/2/16000)
//  // OC1C as Non-Inverted PWM output
//  // TOP = 1000
//  // Bits set:
//  //  - COM = 0b10 (clear output on match when upcounting, set output on match when downcounting)
//  //  - WGM = 0b1010 (wave generation mode = phase correct PWM with ICR4 as the prescaler)
//  //  - CS = 0b001 (Clock source 1, no prescaling)
//  ICR1   = (F_CPU / 8000) / 2;  // Get the prescaler for 8k. Have to divide by two since phase correct.
//  OCR1A  = 0;                    // Set initial duty cycle to 0%
//  TCCR1A = _BV(COM1A1) | _BV(WGM11);
//  TCCR1B = _BV(WGM13) | _BV(CS10);
//
//  pwm_max = ICR1;
//  pinMode(motorB, OUTPUT);
}

/* 16-bit version of analogWrite(). Works only on pins 9 and 10. */
void analogWrite16(uint8_t pin, uint16_t val)
{
  switch (pin) {
    case  9: OCR1A = val; break;
    case 10: OCR1B = val; break;
  }
}

void loop()
{
  timestamp = micros() - start_time;

  /* Speed Control Block
     ----------------------
  */

  error = -radps_adjusted + des_speed;
  sum_error += error;

  //      controller();
  
  pwm = 65535;
  
  
   analogWrite(motorB, HIGH);


  Serial.print(timestamp, DEC);    // Sending the timestamp over to the computer

  Serial.print(",");
  Serial.print(pwm, DEC);    // Sending the timestamp over to the computer

  Serial.print(",");
  Serial.println(radps_adjusted, DEC);   // Comment everything else for reading just the motor speed at the serial receiver

  prev_timestamp = timestamp;

}
