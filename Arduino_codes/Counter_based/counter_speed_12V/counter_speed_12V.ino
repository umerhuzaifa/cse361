/* FreqCount - Example with serial output
 * http://www.pjrc.com/teensy/td_libs_FreqCount.html
 *
 * This example code is in the public domain.
 */

///////////////////////////////////////
#include<FreqCount.h>

#define ppr 550
#define delta_t 30   // equivalent to reading the counter 35 times


#define encoder1 20
#define encoder2 3
#define MICRO_TO_SEC 1000000
#define FIX_TIME  25000 // 10000 

#define motorB 11 //5

#define motor_in1 6  // Active high operation
#define motor_in2 7  // Active high operation

long init_time = micros();
long tstamp = micros();

  unsigned long int count =0;
    unsigned long int prev_count =0;
void setup() {
  Serial.begin(115200);
  FreqCount.begin(delta_t);
  
  pinMode(motor_in1, OUTPUT);
  pinMode(motor_in2, OUTPUT);

  pinMode(47, INPUT);
  pinMode(motorB, OUTPUT);

  digitalWrite(motor_in1, HIGH);
  digitalWrite(motor_in2, LOW);
  analogWrite(motorB, 255);

  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);
}

float filter(){
    return (float) (0.4*prev_count + 0.6 * count);
  }

void pulse_sig(){
    digitalWrite(10, HIGH);
    delay(10);
    digitalWrite(10, LOW);
  }
void loop() {

  if (FreqCount.available()) {
    count= FreqCount.read();
    Serial.print(micros() - init_time);
    Serial.print(",");
    Serial.println((float) filter()*(1000/delta_t)/ppr * 6.28);
    prev_count = count;
    if ()
    pulse_sig();
  }
}
