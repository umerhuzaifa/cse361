/* FreqCount - Example with serial output
 * http://www.pjrc.com/teensy/td_libs_FreqCount.html
 *
 * This example code is in the public domain.
 */

///////////////////////////////////////
#include<FreqCount.h>

#define ppr 550
#define delta_t 30   // equivalent to reading the counter 35 times


#define encoder1 20
#define encoder2 3
#define MICRO_TO_SEC 1000000
#define FIX_TIME  25000 // 10000 

#define motorB 11 //5

#define motor_in1 6  // Active high operation
#define motor_in2 7  // Active high operation
#define pwm_min 50 
#define pwm_max 255

#define pwm_min_pos 35
#define pwm_max_pos 255


#define u_min -10.96
#define u_max 10.96

#define u_min_pos -10
#define u_max_pos 10

long init_time = micros();
long tstamp = micros();

unsigned long int count =0;
unsigned long int prev_count =0;

float cur_pos = 0;
float cur_speed = 0;
float des_speed = 5;
float des_pos = 1.57; // 90 degrees CCW

float u = 0;
int pwm =0;

int meas_Dir;

void CCW()
{
  meas_Dir = 1;

  digitalWrite(motor_in1, LOW);
  digitalWrite(motor_in2, HIGH);
}

void CW()
{
  meas_Dir = -1;
  digitalWrite(motor_in1, HIGH);
  digitalWrite(motor_in2, LOW);
}

void setup() {
  Serial.begin(115200);
  FreqCount.begin(delta_t);
  
  pinMode(motor_in1, OUTPUT);
  pinMode(motor_in2, OUTPUT);

  pinMode(47, INPUT);
  pinMode(motorB, OUTPUT);

  CW();
  pwm = 255;                            // initializing the motor from rest
  analogWrite(motorB, pwm);

  

  pwm = 0;                            // initializing the motor from rest
   analogWrite(motorB, pwm);

  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);

}

float filter(){
    return (float) (0.4*prev_count + 0.6 * count);
  }

void pulse_sig(){
    digitalWrite(10, HIGH);
    delay(10);
    digitalWrite(10, LOW);
  }

void loop() {
  // speed measurement center
  if (FreqCount.available()) {
    count= FreqCount.read();
    Serial.print(micros() - init_time);
    Serial.print(",");
    cur_speed = (float) count*(1000/delta_t)/ppr * 6.28;
    
    prev_count = count;
    
    cur_pos += meas_Dir * cur_speed *delta_t/1000;
    cur_pos = fmod(cur_pos, 6.28);

    u = (des_pos - cur_pos)*2.5 + 1.2 * cur_speed;
    
    if (u<0)
      pwm = map(u, u_min_pos, 0, -pwm_max_pos, -pwm_min_pos);
    else if (u>0)
      pwm = map(u, 0, u_max_pos, pwm_min_pos, pwm_max);
    else
      pwm = 0;
    if (pwm<0)
      CW();      // negative direction of rotation
    else
      CCW();    // positive direction of rotation
  
    Serial.print(cur_speed);
    Serial.print(",");
    Serial.println(cur_pos);
    analogWrite(motorB, abs(pwm));

    pulse_sig();
      cur_pos += meas_Dir * cur_speed *delta_t/1000;
      cur_pos = cur_pos, 6.28;
  // controller design(s)
  
  u = (des_pos - cur_pos)*2.5 + 1.2 * cur_speed;
  if (u<0)
    pwm = map(u, u_min_pos, 0, -pwm_max_pos, -pwm_min_pos);
  else if (u>0)
    pwm = map(u, 0, u_max_pos, pwm_min_pos, pwm_max);
  else
    pwm = 0;
  if (pwm<0)
    CW();      // negative direction of rotation
  else
    CCW();    // positive direction of rotation
//  Serial.print(u);
//  Serial.print(",");
//  Serial.print(pwm);
//  Serial.print(",");
  Serial.print(cur_speed);
  Serial.print(",");
  Serial.println(cur_pos);
  analogWrite(motorB, abs(pwm));

}

}
