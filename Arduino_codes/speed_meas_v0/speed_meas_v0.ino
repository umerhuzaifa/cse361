/* 
 *  Code for finding speed in RPM, rad/s for DC motor GM37-545S using Arduino UNO R3

    Code for playing with the motor at the limiting voltages and reading speed in the meantime

    Umer Huzaifa
    August 12, 2021
    
*/


/* Geared DC motor with Hall Effect Quadrature Encoder
   L298N Motor Driver

   Wiring:
   VCC -> 5V
   GND -> GND
   OUT A -> D2 (external interrupt)
   OUT B -> D3 (external interrupt)
   M1 -> Motor Driver A (Pin 11)
   M2 -> Motor Driver B (None)
   Motor Driver Input 1 -> 3
*/

#include<avr/interrupt.h>

#define encoder1 2
#define encoder2 3

//#define motorA 3
#define motorB 11

#define motorA_brake 9
#define motorB_brake 8

//#define motorA_dir 12
#define motorB_dir 13

/////////////////////////////////////////
/////////////////////////////////////////

volatile int count1 = 0;
volatile int count2 = 0;

int pulses=  100;  // reference number of pulses for finding the speed

int meas_Dir = 0;  // 1 = CW
              // 0 = Stationary
              // -1 = CCW

// Timing the duration between two events:

float time_del = 0;                   // time variable for calculating the delay between two sets of pulses (variable defined above)
float new_time = 0;                   // time variable for the pulse arriving the second time
float init_time = micros();           // a reference for delay calculation
float rpm_meas = 0;                   // revolutions per minute speed
float radps_meas = 0;                 // radians per second speed
float radps_adjusted = 0;             // radians per second found by averaging with last measurement
float prev_filtered = 0;              // last measurement of radians per second
float acc_calc      =0;

float start_time = micros();  //  the startup time, reference for the timestamp
float timestamp = 0;
float prev_timestamp = 0;
// Speed data

float error     = 0;
float des_speed = -8.5;
int pwm         = 0;
int pwm_max     = 255;
int pwm_min     = 20;

float Kp = 4;
float Kd = 2;

/* Defining two step ranges
 * 
 * 
 */

  float step1_start = 1500000;  // 1.5 s
  float step1_end = 10000000;    // 10 s

  float step2_start = 13000000; // 13 s
  float step2_end   = 20000000; // 20 s

  float sim_duration = 20000000; // 20 s
// Low pass filter for the speed

  float alpha = 0.8;

/////////////////////////////////////////
/////////////////////////////////////////
void setup()
{
  Serial.begin(115200);
  // Should it be a pull up input or a simple one??
  pinMode(encoder1, INPUT);
  pinMode(encoder2, INPUT);
  pinMode(motorB_brake, OUTPUT);
  pinMode(motorB_dir, OUTPUT);
  
  attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder2), pulseB, RISING);

  analogWrite(motorB, 0);   // Keeping the motor off in the start
  digitalWrite(motorB_brake, HIGH);
  
  // Initial values of all the speed control variables
  radps_meas = 0;
  prev_filtered = 0;  // seeing if the spike can be avoided by eliminating this reset
  radps_adjusted = 0; 
  
}

void loop() 
{
  while (timestamp <= sim_duration)
  {
  
    /* Speed Measurment Block
     * ----------------------
     */
    
    // Timestamp from the start
    timestamp = micros() - start_time;
    
    
    if ((time_del > 95000) && (digitalRead(motorB_brake) != HIGH)) // if the brakes are not engaged and the time delay is a non-zero value
    {
      
      radps_meas = meas_Dir * (pulses / time_del * 1000000) / (550) * 6.28 ; // i.e. rad/s = (pulses per second)/(pulses per revolution) * 6.28 radians per revolution
      
      // Speed Filter
      /////////////////////
      radps_adjusted = radps_meas  * (1-alpha) + alpha * prev_filtered;
      prev_filtered = radps_adjusted;
      acc_calc      = (radps_adjusted - prev_filtered)/(timestamp - prev_timestamp);
    }
  
    else if (digitalRead(motorB_brake) == HIGH)
    {
      init_time = micros(); // millis();  // re-initializing the initial time to avoid erroneous spike in the speed
      radps_meas = 0;
      prev_filtered = 0;  // seeing if the spike can be avoided by eliminating this reset
      radps_adjusted = 0; 
    }
    
    /* Speed Control Block
     * ----------------------
     */
  
    if ((timestamp < step1_start)|| (timestamp > step1_end && (timestamp < step2_start)))
    {
      pwm = 0;
      digitalWrite(motorB_brake, HIGH);  
    }
    
    else if ((timestamp > step1_start && timestamp < step1_end) || (timestamp > step2_start && timestamp < step2_end))
    { 
      digitalWrite(motorB_brake, LOW);
      error = -radps_adjusted +des_speed;
      Kd = 1;
      Kp = 4;
      controller(error);
  
      if (pwm>=0) 
          digitalWrite(motorB_dir, HIGH); // CCW
      else
          digitalWrite(motorB_dir, LOW); // CW
      
//       pwm = 50;
    }
  
    else
    {
      pwm = 0;
      
      digitalWrite(motorB_brake, HIGH);
     }
  
      analogWrite(motorB, abs(pwm));
       
      // Ready to send the data now to the computer
      Serial.print(timestamp, DEC);    // Sending the timestamp over to the computer
      
      Serial.print(",");
     
      Serial.println(radps_adjusted, DEC);   // Comment everything else for reading just the motor speed at the serial receiver  

      prev_timestamp = timestamp;
  }
}


void controller(float error)
{  
  /////////////////////////////////////////////////////////////////////////
  // discrading map() because it can only handle the integers precisely. //
  //  pwm = map(error, 0, des_speed, 0, 255);                            //
  // pwm = map(error, -des_speed, des_speed, 0, 255);                    //
  /////////////////////////////////////////////////////////////////////////
  
  float Vin, maxVin;
  
  Vin = (Kp * error + Kd * (0 - acc_calc));   // Kp *(des_speed - radps_adjusted) + Kd * (des_acc - curr_acc)
 
  maxVin = 12.6; // maximum value for the voltage in one direction

  
  pwm = (int) pwm_max * Vin / abs(maxVin); // based on the pwm channel bits and the voltage provided

  if (abs(pwm)>pwm_max)
    pwm=pwm_max * sat(pwm);       // if your pwm is beyond the allowed, limit to the allowed value

  else if (abs(pwm)<pwm_min)
    pwm = pwm_min * sat(pwm);
//  Serial.print(error, DEC);
//  Serial.print(",");
//  Serial.print(Vin, DEC);
//  Serial.print(",");
//  Serial.print(pwm, DEC);
//  Serial.print(",");

}

int sat(float s)
{
  if (s>0)
    return 1;
  else if (s<0)
    return -1;
  else
    return 0;
}

void checkDirection() 
{
  if (digitalRead(encoder2) ==  HIGH) {
    meas_Dir = -1; // CW
  }
  else {
    meas_Dir = 1; // CCW
  }
}

void pulseA() 
{
  checkDirection();
  count1 += 1;

  // Measuring speed every 100 pulses from the encoder

  if (abs(count1 - pulses) <= 2)   // instead of (count1 % pulses) to catch as many pulses as we can
  {
    count1 = 0;
    new_time = micros() ;//millis();
    // Time between two pulses is:
    time_del = new_time - init_time;
    // Reinitializing the initial time now
    init_time = new_time;
  }
}

void pulseB() 
{
  // Not needing any operation here.
  //  count2 += Dir;
}
