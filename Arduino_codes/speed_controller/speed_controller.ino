/* Code for finding speed in RPM, rad/s for DC motor GM37-545S using Arduino UNO R3

    Update for v3: Included a low pass filter that averages the last and current speed values
    Umer Huzaifa
    December, 2020

    Updated: Improved the low pass filter to have perfectly non-noisy jumps in the speed to voltage profile
    Umer Huzaifa
    August, 2021
*/

/*
   Update: Add the time stamp with the speed
   Make sure to make changes in the Python code that is reading this stream of data as well
*/


/* Geared DC motor with Hall Effect Quadrature Encoder
   L298N Motor Driver

   Wiring:
   VCC -> 5V
   GND -> GND
   OUT A -> D2 (external interrupt)
   OUT B -> D3 (external interrupt)
   M1 -> Motor Driver A (Pin 11)
   M2 -> Motor Driver B (None)
   Motor Driver Input 1 -> 3

*/

#include<avr/interrupt.h>

#define encoder1 2
#define encoder2 3

//#define motorA 3
#define motorB 11

#define motorA_brake 9
#define motorB_brake 8

//#define motorA_dir 12
#define motorB_dir 13


/////////////////////////////////////////
/////////////////////////////////////////


volatile int count1 = 0;
volatile int count2 = 0;
int pulses=  100;  // reference number of pulses for finding the speed

int meas_Dir = 0;  // 1 = CW
              // 0 = Stationary
              // -1 = CCW

// Timing the duration between two events:

float time_del = 0;                   // time variable for calculating the delay between two sets of pulses (variable defined above)
float new_time = 0;                   // time variable for the pulse arriving the second time
float init_time = micros();           // a reference for delay calculation
float rpm_meas = 0;                   // revolutions per minute speed
float radps_meas = 0;                 // radians per second speed
float radps_adjusted = 0;             // radians per second found by averaging with last measurement
float prev_filtered = 0;              // last measurement of radians per second

float start_time = micros();  //  the startup time, reference for the timestamp
float timestamp = 0;

// Speed data

float error     = 0;
float des_speed = -10;
int pwm         = 0;


/* Defining two step ranges
 * 
 * 
 */
  float step1_start = 3000000;  // 3 s
  float step1_end = 10000000;    // 10 s

  float step2_start = 13000000; // 13 s
  float step2_end   = 20000000; // 20 s

  float sim_duration = 20000000; // 20 s
// Low pass filter for the speed

  float alpha = 0.8;

/////////////////////////////////////////
/////////////////////////////////////////
void setup()
{
  Serial.begin(115200);
  // Should it be a pull up input or a simple one??
  pinMode(encoder1, INPUT);
  pinMode(encoder2, INPUT);
  pinMode(motorB_brake, OUTPUT);
  pinMode(motorB_dir, OUTPUT);
  
  attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder2), pulseB, RISING);

  analogWrite(motorB, 0);   // Keeping the motor off in the start
  digitalWrite(motorB_brake, HIGH);
  
  // Initial values of all the speed control variables
  radps_meas = 0;
  prev_filtered = 0;  // seeing if the spike can be avoided by eliminating this reset
  radps_adjusted = 0; 
  
}

void loop() {

while (timestamp <= sim_duration)
{

  /* Speed Measurment Block
   * ----------------------
   * 
   */
  // Timestamp from the start
  timestamp = micros() - start_time;
  if ((time_del > 95000) && (digitalRead(motorB_brake) != HIGH)) // if the brakes are not engaged and the time delay is a non-zero value
  {
    
    radps_meas = meas_Dir * (pulses / time_del * 1000000) / (550 ) * 6.28 ; // i.e. rad/s = (pulses per second)/(pulses per revolution) * 6.28 radians per revolution

    // Speed Filter
    /////////////////////
    radps_adjusted = radps_meas  * (1-alpha) + alpha * prev_filtered;
    prev_filtered = radps_adjusted;
  }

  else if (digitalRead(motorB_brake) == HIGH)
  {
    init_time = micros(); // millis();  // re-initializing the initial time to avoid erroneous spike in the speed
    radps_meas = 0;
    prev_filtered = 0;  // seeing if the spike can be avoided by eliminating this reset
    radps_adjusted = 0; 
  }
  
  /* Speed Control Block
   * ----------------------
   * 
   */

  if (des_speed<=0)
    digitalWrite(motorB_dir, LOW);
  else
    digitalWrite(motorB_dir, HIGH);
  
  
  
  if ((timestamp < step1_start)|| (timestamp > step1_end && (timestamp < step2_start)))
  {
    
    pwm = 0;
    analogWrite(motorB, pwm);
    digitalWrite(motorB_brake, HIGH);
    
  }
  else if ((timestamp > step1_start && timestamp < step1_end) || (timestamp > step2_start && timestamp < step2_end))
  { 
    error = radps_adjusted - des_speed;
    PWM_map(abs(error));

    analogWrite(motorB, pwm);
    digitalWrite(motorB_brake, LOW);


// In speed control, no need to change the motor direction along the way
//    if (error>=0)
//        digitalWrite(motorB_dir, HIGH);
//    else
//        digitalWrite(motorB_dir, LOW);
//        
  }

  else
  {
    pwm = 0;
    analogWrite(motorB, pwm);
    digitalWrite(motorB_brake, HIGH);
    }
    
  // Ready to send the data now to the computer
  Serial.print(timestamp, 1);    // Sending the timestamp over to the computer
//  Serial.print(",");
//  Serial.print(pwm, 2);
  Serial.print(",");
  Serial.println(radps_adjusted, 8);   // Comment everything else for reading just the motor speed at the serial receiver
  
  
}
}


void PWM_map(float error)
{
 pwm = map(error, 0, abs(des_speed), 0, 255); 
}
  
void checkDirection() {
  if (digitalRead(encoder2) ==  HIGH) {
    meas_Dir = 1;
  }
  else {
    meas_Dir = -1;
  }
  
}

void pulseA() {
  checkDirection();
  count1 += 1;

  // Measuring speed every 100 pulses from the encoder

  if (abs(count1 - pulses) <= 2)   // instead of (count1 % pulses) to catch as many pulses as we can
  {
    count1 = 0;
    new_time = micros() ;//millis();
    // Time between two pulses is:
    time_del = new_time - init_time;
    // Reinitializing the initial time now
    init_time = new_time;

  }
//  else
//  {
//    // do nothing
//  }
}

void pulseB() {
  // Not needing any operation here.
  //  count2 += Dir;
}
