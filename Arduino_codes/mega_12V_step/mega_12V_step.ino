/*


    Code for measuring the motor speed with fixed time period and couting the number of pulses in that time.

    Umer Huzaifa
    August 12, 2021

    Updates --
    20 ms is a decent time for speed measurement
    {4, 3.5, 3.5} are the PID parameters for control

    Updated --
    Done for Atmega 2560

*/


/* Geared DC motor with Hall Effect Quadrature Encoder
   L298N Motor Driver

   Wiring:
   VCC -> 5V
   GND -> GND
   Channel A -> D2 (external interrupt)
   Channel B -> D2 (external interrupt)   
   Pin 9 -> Motor Driver 
   
*/

#include<avr/interrupt.h>
#include<avr/sleep.h>

#define encoder1 20
#define encoder2 3
#define MICRO_TO_SEC 1000000
#define FIX_TIME  250000 // 10000 

#define motorB 11 //5

#define motor_in1 6  // Active high operation
#define motor_in2 7  // Active high operation


/////////////////////////////////////////
/////////////////////////////////////////

volatile int count1 = 0;
volatile int count2 = 0;

uint8_t npulses = 0;   // pulses measured in a given amount of time

int meas_Dir = 0;  // 1 = CW
                  // 0 = Stationary
                  // -1 = CCW

// Timing the duration between two events:

long time_del = 0;                   // time variable for calculating the delay between two sets of pulses (variable defined above)
long new_time = 0;                   // time variable for the pulse arriving the second time
long init_time = micros();           // a reference for delay calculation
long start_time = micros();  //  the startup time, reference for the timestamp
long timestamp = 0;
float prev_timestamp = 0;


// speed variables


float rpm_meas = 0;                   // revolutions per minute speed
float radps_meas = 0;                 // radians per second speed
float radps_adjusted = 0;             // radians per second found by averaging with last measurement
float prev_filtered = 0;              // last measurement of radians per second
float acc_calc      = 0;

// Speed Data

float error     = 0;
float sum_error = 0;
float des_speed = 8;
float ppr       = 550;

uint16_t pwm         = 0;
float pwm_float =0;
uint16_t pwm_max     = 65535;//32767;//65535; // 65535;
uint16_t pwm_min     = 0;//10000;//20000;//-32768;//20000;
uint16_t icr         = 65535; //32767;//0xffff;


// Speed Control Variables

float pid_control = 0;
float maxcontroller = 300;//pwm_max;//1000;
float mincontroller = -300;//pwm_min;//-1000;


float Kp = 4;
float Kd = 4.5;//3.5; // 2.5
float Ki = 4.5; // 2.5;



// Low pass filter for the speed
float run_total = 0;
int buff_size  = 8;
float alpha = 0.8;
float buff_speed[]={0,0,0,0,0,0,0,0};
int buff_ind  = 0;
/////////////////////////////////////////
/////////////////////////////////////////

void brake()
{
  digitalWrite(motor_in1, LOW);
  digitalWrite(motor_in2, LOW);

}

void CCW()
{
  meas_Dir = 1;

  digitalWrite(motor_in1, LOW);
  digitalWrite(motor_in2, HIGH);
}

void CW()
{
  meas_Dir = -1;
  digitalWrite(motor_in1, HIGH);
  digitalWrite(motor_in2, LOW);
}

bool brake_check()
{
  if (digitalRead(motor_in1) && digitalRead(motor_in2) == true)
    return true;
  else
    return false;

}
void filter_reset()
{
  
  }

float filter2(float rad, float p_filt)
{
  float alpha =0.8;
  return (float) rad  * (1 - alpha) + alpha * p_filt;
  }

float filter1(float newVal)
{

  float oldVal = buff_speed[buff_ind];
  buff_ind++;
  buff_speed[buff_ind] = newVal;
  if (buff_ind>=buff_size)
    buff_ind=0;
  for (int i=0; i<buff_size; i++)
      run_total+=buff_speed[i];
  run_total = run_total-oldVal + newVal;
  return run_total/buff_size;
  }
void setup()
{

  
  setupPWM16();
  Serial.begin(115200);
  // Should it be a pull up input or a simple one??

  pinMode(encoder1, INPUT);
  pinMode(encoder2, INPUT);
  pinMode(motor_in1, OUTPUT);
  pinMode(motor_in2, OUTPUT);
  pinMode(motorB, OUTPUT);

  brake();  // starting with a brake


  attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);

  // Initial values of all the speed control variables
  radps_meas = 0;
  prev_filtered = 0;  // seeing if the spike can be avoided by eliminating this reset
  radps_adjusted = 0;
  sum_error     = 0;
  error         = 0;
}

void setupPWM16()
{

  ///////////////////////
    DDRB |= _BV(PB1) | _BV(PB2); //Set pins as outputs 
    TCCR1A = _BV(COM1A1) | _BV(COM1B1) //Non-Inv PWM 
    | _BV(WGM11); // Mode 14: Fast PWM, TOP=ICR1
    TCCR1B = _BV(WGM13) | _BV(WGM12)
    | _BV(CS10); // Prescaler 1
    ICR1 = icr; // TOP counter value (Relieving OCR1A*)
}

/* 16-bit version of analogWrite(). Works only on pins 9 and 10. */
void analogWrite16(uint8_t pin, uint16_t val)
{
  
  switch (pin) {
    case  9: OCR1A = val; break;
    case 10: OCR1B = val; break;
  }
}

void loop()
{
  timestamp = micros() - start_time;

  /* Speed Measurment Block
     ----------------------
  */  


  radps_meas      = (float) meas_Dir * npulses / ppr * 6.28 * MICRO_TO_SEC / FIX_TIME;
  radps_adjusted = filter2(radps_meas, prev_filtered);
  acc_calc        =  (float) (radps_adjusted - prev_filtered) * MICRO_TO_SEC/ FIX_TIME; //
  prev_filtered   = radps_adjusted;


  /* Speed Control Block
     ----------------------
  */

  error = -radps_adjusted + des_speed;
  sum_error += error;
//
//
//        controller();

  // when not using the controller
  pwm = 65535;

  
  if (pid_control >= 0)   // i.e. the des_speed is bigger, spin faster
    CCW();
  else            // i.e. the des_speed is lower, spin the other way
    CW();
    
  analogWrite(motorB, abs(pwm));
  prev_timestamp = timestamp;
  if (timestamp<=2e6)
  display(1);
  else{
  Serial.end();
  sleep_mode();
  }
}

void display(int mode)

{
  switch (mode)
  {
    case 1: 
    // For sending the data to the computer for Python plotting
      
      Serial.print(timestamp, DEC);   // Comment everything else for reading just the motor speed at the serial receiver
      Serial.print(" ");
      Serial.print(radps_adjusted, DEC);    // Sending the timestamp over to the computer
      Serial.println();
    break;


    case 2:
        Serial.print(timestamp, DEC);    // Sending the timestamp over to the computer
        Serial.print(",");
        Serial.print(error, DEC);
        Serial.print(",");
        Serial.print(acc_calc, DEC);
        Serial.print(",");  
        Serial.print(sum_error, DEC);
        Serial.print(",");  
        Serial.print(pid_control, DEC);
        Serial.print(",");
        Serial.print(pwm, DEC);
        Serial.print(",");
        Serial.println(radps_adjusted, DEC);   // Comment everything else for reading just the motor speed at the serial receiver
    break;
    }
  
  }

void controller()
{
  /////////////////////////////////////////////////////////////////////////
  // discrading map() because it can only handle the integers precisely. //
  //  pwm = map(error, 0, des_speed, 0, 255);                            //
  // pwm = map(error, -des_speed, des_speed, 0, 255);                    //
  /////////////////////////////////////////////////////////////////////////
 

pid_control = (Kp * error + Kd * (0 - acc_calc)  + Ki * sum_error);   // Kp *(des_speed - radps_adjusted) + Kd * (des_acc - curr_acc)

  
  
  pid_control = constrain(pid_control, mincontroller, maxcontroller);
    pwm = map16(abs(pid_control), 0, maxcontroller, pwm_min, pwm_max);

  if (abs(pwm) > pwm_max)
    pwm = pwm_max * sat(pwm);     // if your pwm is beyond the allowed, limit to the allowed value

  else if (abs(pwm) < pwm_min)
    pwm = pwm_min * sat(pwm);

  
}

int sat(int s)
{
  if (s > 0)
    return 1;
  else if (s < 0)
    return -1;
  else
    return 0;
}



// The values coming out are not making any sense at all. I have tried calculations of the expression by hand as well 
// and still the answers are not matching.

// Make sure to compute the answer in float and then pass on as a uint16_t
////////////////////////////////////////////////////////////////////////////////

uint16_t  map16(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max) {
  float result;
  result = (float)(x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
//  Serial.print("Mapping: ");
//  Serial.print(result, DEC);
//  Serial.print(", ");
  return result;

}
////////////////////////////////////////////////////////////////////////////////
void pulseA()
{
//  checkDirection();

  new_time = micros();
  time_del = new_time - init_time;

  if (time_del <= FIX_TIME)
  {
    count1 += 1;
  }
  else
  {

    npulses = count1;

    init_time = new_time;
    count1 = 0;
  }

}

//void pulseB()
//{
//  // Not needing any operation here.
//  //  count2 += Dir;
//}
