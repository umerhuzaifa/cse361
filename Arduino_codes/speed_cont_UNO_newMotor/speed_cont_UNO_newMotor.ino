/* 
 *
 *
 *  Code for measuring the motor speed with fixed time period and couting the number of pulses in that time.
 *  
 *  
    Umer Huzaifa
    August 12, 2021
    
*/


/* Geared DC motor with Hall Effect Quadrature Encoder
   L298N Motor Driver

   Wiring:
   VCC -> 5V
   GND -> GND
   OUT A -> D2 (external interrupt)
   OUT B -> D3 (external interrupt)
   M1 -> Motor Driver A (Pin 11)
   M2 -> Motor Driver B (None)
   Motor Driver Input 1 -> 3
*/

#include<avr/interrupt.h>

#define encoder1 2
#define encoder2 3
#define MICRO_TO_SEC 1000000   // conversion factor of microsec to seconds
#define FIX_TIME  20000

#define motorB 11

#define motorA_brake 9
#define motorB_brake 8

#define motorB_dir 13

/////////////////////////////////////////
/////////////////////////////////////////

volatile int count1 = 0;
volatile int count2 = 0;
uint8_t npulses = 0;   // pulses measured in a given amount of time

uint8_t meas_Dir = 0;  // 1 = CW
              // 0 = Stationary
              // -1 = CCW

// Timing the duration between two events:

long start_time = micros();  //  the startup time, reference for the timestamp
long time_del = 0;                   // time variable for calculating the delay between two sets of pulses (variable defined above)
long new_time = 0;                   // time variable for the pulse arriving the second time
long init_time = micros();           // a reference for delay calculation
long timestamp = 0;
long prev_timestamp = 0;

// Speed variables:

float rpm_meas = 0;                   // revolutions per minute speed
float radps_meas = 0;                 // radians per second speed
float radps_adjusted = 0;             // radians per second found by averaging with last measurement
float prev_filtered = 0;              // last measurement of radians per second
float acc_calc      =0;
// Low pass filter for the speed

float alpha = 0.8;


// Speed data

float error     = 0;
float sum_error = 0;
float des_speed = -8.5;
float PPR         = 751.8;

uint16_t pwm         = 0;
uint16_t pwm_max     = 255;// 65535;
uint16_t pwm_min     = 120;

float Kp = 4;
float Kd = 0;//2.5;  
float Ki = 0; //2.0;

/* Defining two step ranges
 * 
 * 
 */

  float step1_start = 1500000;  // 1.5 s
  float step1_end = 10000000;    // 10 s

  float step2_start = 13000000; // 13 s
  float step2_end   = 20000000; // 20 s

  float sim_duration = 20000000; // 20 s


/////////////////////////////////////////
/////////////////////////////////////////

  void setup()
  {

    Serial.begin(115200);
    // Should it be a pull up input or a simple one??
    pinMode(encoder1, INPUT);
    pinMode(encoder2, INPUT);
    pinMode(motorB, OUTPUT);
    pinMode(motorB_brake, OUTPUT);
    pinMode(motorB_dir, OUTPUT);
    
    attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);
    attachInterrupt(digitalPinToInterrupt(encoder2), pulseB, RISING);
  
    analogWrite(motorB, 0);   // Keeping the motor off in the start
    digitalWrite(motorB_brake, HIGH);
    
    // Initial values of all the speed control variables
    radps_meas = 0;
    prev_filtered = 0;  // seeing if the spike can be avoided by eliminating this reset
    radps_adjusted = 0; 
    sum_error     = 0;
    error         = 0;
  }


 
    void loop() 
    {
      while (timestamp <= sim_duration)
      {
      
        /* Speed Measurment Block
         * ----------------------
         */  
        // Timestamp from the start
        timestamp = micros() - start_time;
    
        if (digitalRead(motorB_brake) != HIGH)
            {        
              radps_meas      = (float) meas_Dir * npulses/PPR * 6.28 * MICRO_TO_SEC/FIX_TIME;
              radps_adjusted  = (float) radps_meas  * (1-alpha) + alpha * prev_filtered;    
              acc_calc        =  (float) (radps_adjusted - prev_filtered)/FIX_TIME;   //   
              prev_filtered   = radps_adjusted;
            }
        else
            {
              init_time  = micros();
              count1     = 0;
              radps_meas = 0;
              prev_filtered = 0;
              radps_adjusted = 0;
              sum_error  = 0;
              }    
        /* Speed Control Block
         * ----------------------
         */
      
        if ((timestamp < step1_start)|| (timestamp > step1_end && (timestamp < step2_start)))
            {
              pwm = 0;
              digitalWrite(motorB_brake, HIGH);  
            }
            
        else if ((timestamp > step1_start && timestamp < step1_end) || (timestamp > step2_start && timestamp < step2_end))
            { 
              digitalWrite(motorB_brake, LOW);
              error = -radps_adjusted +des_speed;
              sum_error += error;
              controller();
        
              if (pwm>=0) 
                  digitalWrite(motorB_dir, HIGH); // CCW
              else
                  digitalWrite(motorB_dir, LOW); // CW
              
            }
      
        else
            {
              pwm = 0;
              
              digitalWrite(motorB_brake, HIGH);
             }
    
         
          analogWrite(motorB, abs(pwm));
           
          // Ready to send the data now to the computer
          Serial.print(timestamp, DEC);    // Sending the timestamp over to the computer
          
          Serial.print(",");

          Serial.print(pwm);
          Serial.print(",");
          Serial.println(radps_adjusted, DEC);   // Comment everything else for reading just the motor speed at the serial receiver  
    
          prev_timestamp = timestamp;
      }
    }


    void controller()
    {   
      /////////////////////////////////////////////////////////////////////////
      // discrading map() because it can only handle the integers precisely. //
      //  pwm = map(error, 0, des_speed, 0, 255);                            //
      // pwm = map(error, -des_speed, des_speed, 0, 255);                    //
      /////////////////////////////////////////////////////////////////////////
      
      float Vin, maxVin;
      
      Vin = (Kp * error + Kd * (0 - acc_calc)  + Ki * sum_error);   // Kp *(des_speed - radps_adjusted) + Kd * (des_acc - curr_acc)
     
      maxVin = 12.6; // maximum value for the voltage in one direction
    
      
      pwm = (uint16_t) pwm_max * Vin / abs(maxVin); // based on the pwm channel bits and the voltage provided
  
    
      if (abs(pwm)>pwm_max)
        pwm=pwm_max * sat(pwm);       // if your pwm is beyond the allowed, limit to the allowed value
    
      else if (abs(pwm)<pwm_min)
        pwm = pwm_min * sat(pwm);

      
   
    //      pwm = 120;
    //
    //  Serial.print(error, DEC);
    //  Serial.print(",");
    //  Serial.print(sum_error, DEC);
    //  Serial.print(",");
    //  Serial.print(Vin, DEC);
    //  Serial.print(",");
    //  Serial.print(pwm, DEC);
    //  Serial.print(",");
    
    
    }

    uint16_t sat(uint16_t s)
    {
      if (s>0)
        return 1;
      else if (s<0)
        return -1;
      else
        return 0;
    }
    

    void checkDirection() 
    {
      if (digitalRead(encoder2) ==  HIGH) {
        meas_Dir = -1; // CW
      }
      else 
        meas_Dir = 1; // CCW
      
    }

/*
 * long prev_t, t_now, one_sec;
 * 
 */

    void pulseA() 
    {
        new_time = micros();
        time_del = new_time - init_time;
    
        if (time_del <= FIX_TIME)
            count1 +=1;         
        else
          {            
            npulses = count1;
            checkDirection();
            init_time = new_time;
            count1 = 0;
          }        
    }

void pulseB() 
{
  // Not needing any operation here.
  //  count2 += Dir;
}
