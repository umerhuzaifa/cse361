# Code for writing the speed profile of the motor given a step input of data.
# Umer Huzaifa
# 1/8/2020
# Code for reading the serial data from Motor and saving as a txt file
# Update:
# After adding the timestamp from v4 Arduino code, need to parse the message
# here too



#!/usr/bin/env python

from threading import *
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct
import pandas as pd
import sys

class serialPlot:
    def __init__(self, serialPort = 'COM3', serialBaud = 115200, plotLength = 100, dataNumBytes = 2):
        self.port = serialPort
        self.baud = serialBaud
        self.plotMaxLength = plotLength
        self.data = []
        self.time = []
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        self.plotTimer = 0
        self.previousTimer = 0
        self.timerflag = 0
        self.startTime = 0
        print('Trying to connect to: ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        try:
            self.serialConnection = serial.Serial(serialPort, serialBaud, timeout=4)
            print('Connected to ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        except:
            print("Failed to connect with " + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')

    def readSerialStart(self):
        
        if self.thread == None:
            self.thread = Thread(target=self.backgroundThread)
            
            self.thread.start()
            
            # Block till we start receiving values
            while self.isReceiving != True:
                time.sleep(0.1)
           
    def getSerialData(self):        
        currentTimer = time.time()
        duration = currentTimer - self.startTime -0.01396    # Adjusting the time value by a factor I have repeatedly found
##        print(duration)
        
        if abs(duration-5) <=0.001:   # Read the data for only 5 seconds to complete the data acquisition
##            self.isRun=False
            self.isRun = True
        else:
            self.plotTimer = int((currentTimer - self.previousTimer) * 1000)     # the first reading will be erroneous
            self.previousTimer = currentTimer
            self.time.append(float(self.rawData[:8]))
            self.data.append(float(self.rawData[9:-2]))
            print('{}'.format(float(self.time[-1])))
            print(self.rawData[9:-2])
            data_arr = self.rawData.split(",")
            print(type(self.rawData))
            print("{}, {}".format(data_arr[0], data_arr[1]))
            value = float(self.rawData[0:-2])   # Removing carriage return and newline escape characters from the string
            print(currentTimer, self.rawData)
            self.time.append(duration)
            self.data.append(value)    # we get the latest data point and append it to our array            
        
    def backgroundThread(self):    # retrieve data
        self.serialConnection.reset_input_buffer()
        
        while (self.isRun):
            self.rawData = self.serialConnection.readline()
            self.isReceiving = True

    def close(self):
        self.isRun = False
        self.thread.join()
        self.serialConnection.close()
        print('Disconnected...')
        
    
##        df = pd.DataFrame(self.data)
##        df.to_csv('encoder_read.csv')
##        print('Finished writing CSV file too')
        
##        infile1 = open('encoder_read.txt','w')
##        infile2 = open('encoder_time.txt','w')
##        print(self.data)
##        for i in range(len(self.data)):
##            print(self.time[i], self.data[i])
##            infile1.write(str(self.data))
##            infile2.write(str(self.time))
##        infile1.close()
##        infile2.close()
        
def main():
    portName = 'COM3'     # for windows users
    baudRate = 115200
    maxPlotLength = 1000
    dataNumBytes = 3      # number of bytes of 1 data point
    s = serialPlot(portName, baudRate, maxPlotLength, dataNumBytes)   # initializes all required variables
   ## s.startTime = time.time();
##    print(s.startTime)
    s.readSerialStart()# starts background thread

    s.startTime = time.time()
    while s.isRun:
        s.getSerialData()
        fargs=(axes, lines, lineValueText, lineLabel, timeText)   # fargs has to be a tuple

    plt.legend(loc="upper left")
    plt.show()

    s.close()
    end_time = time.time()
    print('It actually took {} seconds'.format(end_time - s.startTime))
if __name__ == '__main__':
    main()
