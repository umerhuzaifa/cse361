clear all
close all
clc

% Connection to the serial port that is receiving the data from arduino

device = serialport('COM3', 115200);
figure
hold on
data = [];
while (1)
    str2double(readline(device))
    data = [data str2double(readline(device))];
    plot(data) 
    drawnow
% str2double(readline(device))
end
% Whenever you need to modify the Arduino code, you need to clear the
% device here so that serial communication is ended first.