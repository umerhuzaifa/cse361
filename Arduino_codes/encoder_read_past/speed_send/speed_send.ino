/* Code for finding speed in RPM, rad/s for DC motor GM37-545S using Arduino UNO R3
 *  
 *  Update for v3: Included a low pass filter that averages the last and current speed values
 *  Umer Huzaifa
 *  December 2020
 */


/* Geared DC motor with Hall Effect Quadrature Encoder 
 * L298N Motor Driver
 * 
 * Wiring:
 * VCC -> 5V
 * GND -> GND
 * OUT A -> D2 
 * OUT B -> D3 
 * M1 -> Motor Driver A
 * M2 -> Motor Driver B
 * Motor Driver Input 1 -> 3

 */     
#include<avr/interrupt.h>

#define encoder1 2
#define encoder2 3
//#define motorA 3
#define motorB 11

#define motorA_brake 9
#define motorB_brake 8

volatile int count1 = 0;
volatile int count2 = 0;
//volatile bool flag =0; 
int Dir = 0;  // 1 = CW
              // 0 = Stationary
              // -1 = CCW
// Timing the duration between two events:
float time_del = 0;
float new_time = 0;
float init_time = micros();//millis();
float rpm_meas = 0; // revolutions per minute speed
float radps_meas = 0; // radians per second speed
float radps_adjusted = 0; // radians per second found by averaging with last measurement
float prev_radps_meas = 0; // last measurement of radians per second
int N = 50;  // number of pulses passed after which the speed is computed

void setup() 
{
  Serial.begin(115200);
  pinMode(encoder1, INPUT_PULLUP);
  pinMode(encoder2, INPUT_PULLUP);
  pinMode(motorB_brake, INPUT);
  pinMode(motorB, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder2), pulseB, RISING);
  digitalWrite(motorB, HIGH);
}

void loop() {
  
  // Adding some debouncing strategy: 
  // Only measure the rotation speed if the pulses are decently apart

  Dir = 1;   // Since I havent yet figured out how to handle the direction piece. 
  if ((time_del>=2000) && (digitalRead(motorB_brake)!=HIGH)){
    
  // rpm_meas = Dir * (N/time_del * 1000)/(550 ) * 60 ; // time_del in ms:  i.e. rpm = (pulses per second)/(pulses per revolution) * 60 seconds per minute
    rpm_meas = Dir * (N/time_del * 1000000)/(550 ) * 60 ; // time_del in micro sec: i.e. rpm = (pulses per second)/(pulses per revolution) * 60 seconds per minute
  
  //  Serial.print("Measured speed in RPM is: ");
  //  Serial.println(rpm_meas);
  
    radps_meas = rpm_meas * 6.28 / 60;  // i.e. rad/s = revolutions per minute * 6.28 radians per revolution / 60 seconds per minute
  //  Serial.print("Measured speed in rad/s is: ");
    radps_adjusted = radps_meas * 0.6 + prev_radps_meas * 0.4;
    prev_radps_meas = radps_adjusted;
  }
  else if (digitalRead(motorB_brake)==HIGH)
  {
//    init_time =micros();// millis();  // re-initializing the initial time to avoid erroneous spike in the speed
    rpm_meas = 0;
    radps_meas = 0;
    prev_radps_meas = 0;
    radps_adjusted =0;
    //    Serial.println("Yes, we were halted!");
    }
 int t=0;
 
 t=Serial.println(radps_adjusted, 8);   // Comment everything else for reading just the motor speed at the serial receiver
//  Serial.println(t); // Checks how many bytes are being sent through serial communication.
}

void checkDirection(){
  if (digitalRead(encoder2) ==  HIGH){                             
    Dir = 1;  
  }
  else{
    Dir = -1;
  }
//Dir = 1;
}

void pulseA(){  
  

   // Measuring speed every N pulses from the encoder
        count1+= 1;
  if (count1%N ==0)
  {
    checkDirection();
    count1 = 0;
    new_time = micros();//millis(); 
    // Time between two sets of N pulses is:
    time_del = new_time - init_time;
    // Reinitializing the initial time now
    init_time = new_time;
    }

}

void pulseB(){  
  // Not needing any operation here.
 //  count2 += Dir;
}
