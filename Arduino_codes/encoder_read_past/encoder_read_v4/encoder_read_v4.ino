/* Code for finding speed in RPM, rad/s for DC motor GM37-545S using Arduino UNO R3

    Update for v3: Included a low pass filter that averages the last and current speed values
    Umer Huzaifa
    December 2020
*/

/*
   Update: Add the time stamp with the speed
   Make sure to make changes in the Python code that is reading this stream of data as well
*/


/* Geared DC motor with Hall Effect Quadrature Encoder
   L298N Motor Driver

   Wiring:
   VCC -> 5V
   GND -> GND
   OUT A -> D2
   OUT B -> D3
   M1 -> Motor Driver A
   M2 -> Motor Driver B
   Motor Driver Input 1 -> 3

*/
#include<avr/interrupt.h>

#define encoder1 2
#define encoder2 3

//#define motorA 3
#define motorB 11

#define motorA_brake 9
#define motorB_brake 8

#define motorA_dir 12
#define motorB_dir 13

volatile int count1 = 0;
volatile int count2 = 0;
//volatile bool flag =0;
int Dir = 0;  // 1 = CW
              // 0 = Stationary
              // -1 = CCW

// Timing the duration between two events:

float time_del = 0;                   // time variable for calculating the delay between two pulses
float new_time = 0;                   // time variable for the pulse arriving the second time
float init_time = micros();//millis();   // a reference for delay calculation
float rpm_meas = 0; // revolutions per minute speed
float radps_meas = 0; // radians per second speed
float radps_adjusted = 0; // radians per second found by averaging with last measurement
float prev_radps_meas = 0; // last measurement of radians per second

float start_time = micros();  //  the startup time, reference for the timestamp
float timestamp = 0;

void setup()
{
  Serial.begin(9600);
  // Should it be a pull up input or a simple one??
  pinMode(encoder1, INPUT);
  pinMode(encoder2, INPUT);
  pinMode(motorB_brake, OUTPUT);
  pinMode(motorB_dir, OUTPUT);
  
  attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder2), pulseB, RISING);

  analogWrite(motorB, 0);   // Keeping the motor off in the start
  digitalWrite(motorB_dir, LOW);
  digitalWrite(motorB_brake, LOW);
}

void loop() {

  // Timestamp from the start for keeping track of the speed values
  timestamp = micros() - start_time;
  if ((time_del != 0) && (digitalRead(motorB_brake) != HIGH)) // if the brakes are not engaged and the time delay is a non-zero value
  {
    //    rpm_meas = Dir * (100/time_del * 1000)/(550 ) * 60 ; // i.e. rpm = (pulses per second)/(pulses per revolution) * 60 seconds per minute
    rpm_meas = Dir * (100 / time_del * 1000000) / (550 ) * 60 ; // i.e. rpm = (pulses per second)/(pulses per revolution) * 60 seconds per minute. Note 100 pulses and microsec units accomdodated
    //  Serial.print("Measured speed in RPM is: ");
    //  Serial.println(rpm_meas);
    radps_meas = rpm_meas * 6.28 / 60;  // i.e. rad/s = revolutions per minute * 6.28 radians per revolution / 60 seconds per minute
    //  Serial.print("Measured speed in rad/s is: ");
    radps_adjusted = radps_meas * 0.6 + prev_radps_meas * 0.4;

    prev_radps_meas = radps_adjusted;
  }

  else if (digitalRead(motorB_brake) == HIGH)
  {
    init_time = micros(); // millis();  // re-initializing the initial time to avoid erroneous spike in the speed
    rpm_meas = 0;
    radps_meas = 0;
    prev_radps_meas = 0;
    radps_adjusted = 0;
    //    Serial.println("Yes, we were halted!");
  }
//   if ((timestamp>1000000) && (timestamp<25000000))
  if ((timestamp > 25000000))
  {
    digitalWrite(motorB_brake, HIGH);
  }
  else
  {

  }

  // Ready to send the data now to the computer
    Serial.print(timestamp, 1);    // Sending the timestamp over to the computer
    Serial.print(",");
    Serial.println(radps_adjusted, 8);   // Comment everything else for reading just the motor speed at the serial receiver
  
  analogWrite(motorB, 255);
 

}

void checkDirection() {
  if (digitalRead(encoder2) ==  HIGH) {
    Dir = 1;
  }
  else {
    Dir = -1;
  }

}

void pulseA() {
  checkDirection();
  count1 += 1;

  // Measuring speed every 100 pulses from the encoder to avoid any infinity or abnormal values

  if (count1 % 100 == 0)
  {
    count1 = 0;
    new_time = micros();//millis();
    // Time between two pulses is:
    time_del = new_time - init_time;
    // Reinitializing the initial time now
    init_time = new_time;

  }
  else
  {
    // do nothing
  }
}

void pulseB() {
  // Not needing any operation here.
  //  count2 += Dir;
}
