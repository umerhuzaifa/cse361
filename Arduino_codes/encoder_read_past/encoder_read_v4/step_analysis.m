% On plotting the saved data and analyzing for finding the first order
% system parameters


% Umer Huzaifa
% 7/14/2021

clear all
close all

T = readtable('motor_step.csv');
t = T{:, 1};  % note that the time stamps start from 0.225 s instead of 0 s
v = T{:, 2};

t_step = t(5:20)-t(5);
v_step = v(5:20);
plot(t_step, v_step)

title('Step Response of a Motor', 'interpreter', 'latex')
xlabel('Time (sec)', 'interpreter', 'latex')
ylabel('Angular Velocity (rad/s)', 'interpreter', 'latex')

set(gca,'FontSize', 25,'TickLabelInterpreter','latex')
 
set(gcf, 'Color', 'white')