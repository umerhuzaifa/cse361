/* The purpose of the code is to plot and send the encoder data converted to rad/s. 
 *  The power connection to the motor and the encoder connection to pins 2 and 3 are assumed here.
 */


/* Geared DC motor with Hall Effect Quadrature Encoder 
 * L298N Motor Driver
 * 
 * Wiring:
 * VCC -> 5V
 * GND -> GND
 * OUT A -> D2 
 * OUT B -> D3 
 * M1 -> Motor Driver A
 * M2 -> Motor Driver B
 * Motor Driver Input 1 -> 3

 */     
#include<avr/interrupt.h>
#define encoder1 2
#define encoder2 3

#define motorA 3
#define motorB 11

#define motorA_dir 12
#define motorB_dir 13

#define motorA_brake 9
#define motorB_brake 8

volatile int count1 = 0;
volatile int count2 = 0;
volatile bool flag =0;
int Dir = 0;  // 1 = CW
              // 0 = Stationary
              // -1 = CCW
              
// Timing the duration between two events:
float time_del = 0;
float new_time = 0;
float init_time = millis();
float rpm_meas = 0; // revolutions per minute speed
float radps_meas = 0; // radians per second speed

void setup() 
{
  Serial.begin(115200);
  
  pinMode(encoder1, INPUT);
  pinMode(encoder2, INPUT);
  
  pinMode(motorA, OUTPUT);
  pinMode(motorA_dir, OUTPUT);
  pinMode(motorA_brake, OUTPUT);

//  attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder2), pulseB, RISING);
 
}

void loop() {

  
  
  if (flag==1)
  {
//     Serial.print("Direction is: ");
  if (Dir == 1)
  {
//  Serial.println("Clockwise");
  }
  else if (Dir == -1)
  {
//   Serial.println("Counter Clockwise");
    }
//  Serial.println("One Revolution completed");
//  Serial.print("Time taken in milliseconds: ");
//  Serial.println(time_del);
//  rpm_meas = Dir * 1/time_del * 1000*60;
//  Serial.print("Measured speed in RPM is: ");
//  Serial.println(rpm_meas);
  radps_meas = Dir * 6.28 / time_del * 1000;
//  Serial.print("Measured speed in rad/s is: ");
  Serial.println(radps_meas);   // Comment everything else for reading just the motor speed at the serial receiver
  flag = 0;
  }


  digitalWrite(motorA_dir, LOW);   // direction deciding
  digitalWrite(motorA_brake, LOW);   // disengaging the brakes
  analogWrite(motorA, 255); // running the motor at full speed

 delay(3000);

  digitalWrite(motorA_brake, HIGH);   // disengaging the brakes
 
}

void checkDirection(){
  if(digitalRead(encoder2) ==  HIGH){                             
    Dir = 1;  
  }
  else{
    Dir = -1;
  }
}

void pulseB(){  
  checkDirection();
  count1+= Dir;
  
  // Based on the gear reduction ratio of 1:90, the 
  // total number of pulses for one rotation are
  // 11 PPR * 90 = 990 pulses per revolution
  if (count1%550 ==0)
  {
    flag = 1;
    count1 = 0;
    new_time = millis(); 
    time_del = new_time - init_time;
    init_time = new_time;

    }
}
