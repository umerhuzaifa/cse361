/* Code for finding speed in RPM, rad/s for DC motor GM37-545S using Arduino UNO R3
 *  
 *  Umer Huzaifa
 *  December 2020
 */


/* Geared DC motor with Hall Effect Quadrature Encoder 
 * L298N Motor Driver
 * 
 * Wiring:
 * VCC -> 5V
 * GND -> GND
 * OUT A -> D2 
 * OUT B -> D3 
 * M1 -> Motor Driver A
 * M2 -> Motor Driver B
 * Motor Driver Input 1 -> 3

 */     
#include<avr/interrupt.h>

#define encoder1 2
#define encoder2 3
//#define motorA 3
#define motorB 11

#define motorA_brake 9
#define motorB_brake 8

volatile int count1 = 0;
volatile int count2 = 0;
//volatile bool flag =0; 
int Dir = 0;  // 1 = CW
              // 0 = Stationary
              // -1 = CCW
// Timing the duration between two events:
float time_del = 0;
float new_time = 0;
float init_time = millis();
float rpm_meas = 0; // revolutions per minute speed
float radps_meas = 0; // radians per second speed
void setup() 
{
  Serial.begin(115200);
  pinMode(encoder1, INPUT);
  pinMode(encoder2, INPUT);
  pinMode(motorB_brake, INPUT);
  pinMode(motorB, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(encoder1), pulseA, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder2), pulseB, RISING);
  digitalWrite(motorB, HIGH);
}

void loop() {
  
//  Serial.println("Time between 100 pulses (in seconds):");
//  Serial.println(time_del/1000);

  if ((time_del!=0) && (digitalRead(motorB_brake)!=HIGH)){
    rpm_meas = Dir * (100/time_del * 1000)/(550 ) * 60 ; // i.e. rpm = (pulses per second)/(pulses per revolution) * 60 seconds per minute
  //  Serial.print("Measured speed in RPM is: ");
  //  Serial.println(rpm_meas);
    radps_meas = rpm_meas * 6.28 / 60;  // i.e. rad/s = revolutions per minute * 6.28 radians per revolution / 60 seconds per minute
  //  Serial.print("Measured speed in rad/s is: ");
    
  }
  else if (digitalRead(motorB_brake)==HIGH)
  {
    init_time = millis();  // re-initializing the initial time to avoid erroneous spike in the speed
    rpm_meas = 0;
    radps_meas = 0;
    //    Serial.println("Yes, we were halted!");
    }
 Serial.println(radps_meas);   // Comment everything else for reading just the motor speed at the serial receiver
 
}

void checkDirection(){
  if (digitalRead(encoder2) ==  HIGH){                             
    Dir = 1;  
  }
  else{
    Dir = -1;
  }
}

void pulseA(){  
  checkDirection();
  count1+= 1;

   // Measuring speed every 100 pulses from the encoder
  
  if (count1%100 ==0)
  {
    count1 = 0;
    new_time = millis(); 
    // Time between two pulses is:
    time_del = new_time - init_time;
    // Reinitializing the initial time now
    init_time = new_time;
    }
    else
    {
      
      }
}

void pulseB(){  
  // Not needing any operation here.
 //  count2 += Dir;
}
