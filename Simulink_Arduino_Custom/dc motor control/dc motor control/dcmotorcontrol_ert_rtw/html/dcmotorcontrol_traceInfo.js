function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <Root>/EN-1 */
	this.urlHashMap["dcmotorcontrol:5"] = "dcmotorcontrol.c:35,45&dcmotorcontrol.h:78,110&dcmotorcontrol_data.c:40";
	/* <Root>/EN-1 Display */
	this.urlHashMap["dcmotorcontrol:10"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=dcmotorcontrol:10";
	/* <Root>/EN-2 */
	this.urlHashMap["dcmotorcontrol:9"] = "dcmotorcontrol.c:63,73&dcmotorcontrol.h:79,113&dcmotorcontrol_data.c:43";
	/* <Root>/EN-2 Display */
	this.urlHashMap["dcmotorcontrol:11"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=dcmotorcontrol:11";
	/* <Root>/GND1 */
	this.urlHashMap["dcmotorcontrol:6"] = "dcmotorcontrol.c:36&dcmotorcontrol.h:104&dcmotorcontrol_data.c:34";
	/* <Root>/GND2 */
	this.urlHashMap["dcmotorcontrol:8"] = "dcmotorcontrol.c:64&dcmotorcontrol.h:98&dcmotorcontrol_data.c:28";
	/* <Root>/Scope */
	this.urlHashMap["dcmotorcontrol:12"] = "dcmotorcontrol.h:86";
	/* <Root>/VCC1 */
	this.urlHashMap["dcmotorcontrol:4"] = "dcmotorcontrol.c:37&dcmotorcontrol.h:107&dcmotorcontrol_data.c:37";
	/* <Root>/VCC2 */
	this.urlHashMap["dcmotorcontrol:7"] = "dcmotorcontrol.c:65&dcmotorcontrol.h:101&dcmotorcontrol_data.c:31";
	/* <S1>/Data Type Conversion */
	this.urlHashMap["dcmotorcontrol:2:114"] = "dcmotorcontrol.c:47,58";
	/* <S1>/Digital Output */
	this.urlHashMap["dcmotorcontrol:2:214"] = "dcmotorcontrol.c:60,189&dcmotorcontrol.h:92&dcmotorcontrol_data.c:22";
	/* <S2>/Data Type Conversion */
	this.urlHashMap["dcmotorcontrol:3:114"] = "dcmotorcontrol.c:75,86";
	/* <S2>/Digital Output */
	this.urlHashMap["dcmotorcontrol:3:214"] = "dcmotorcontrol.c:88,192&dcmotorcontrol.h:95&dcmotorcontrol_data.c:25";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "dcmotorcontrol"};
	this.sidHashMap["dcmotorcontrol"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>"] = {sid: "dcmotorcontrol:2"};
	this.sidHashMap["dcmotorcontrol:2"] = {rtwname: "<S1>"};
	this.rtwnameHashMap["<S2>"] = {sid: "dcmotorcontrol:3"};
	this.sidHashMap["dcmotorcontrol:3"] = {rtwname: "<S2>"};
	this.rtwnameHashMap["<Root>/EN-1"] = {sid: "dcmotorcontrol:5"};
	this.sidHashMap["dcmotorcontrol:5"] = {rtwname: "<Root>/EN-1"};
	this.rtwnameHashMap["<Root>/EN-1 Digital Output"] = {sid: "dcmotorcontrol:2"};
	this.sidHashMap["dcmotorcontrol:2"] = {rtwname: "<Root>/EN-1 Digital Output"};
	this.rtwnameHashMap["<Root>/EN-1 Display"] = {sid: "dcmotorcontrol:10"};
	this.sidHashMap["dcmotorcontrol:10"] = {rtwname: "<Root>/EN-1 Display"};
	this.rtwnameHashMap["<Root>/EN-2"] = {sid: "dcmotorcontrol:9"};
	this.sidHashMap["dcmotorcontrol:9"] = {rtwname: "<Root>/EN-2"};
	this.rtwnameHashMap["<Root>/EN-2 Digital Output1"] = {sid: "dcmotorcontrol:3"};
	this.sidHashMap["dcmotorcontrol:3"] = {rtwname: "<Root>/EN-2 Digital Output1"};
	this.rtwnameHashMap["<Root>/EN-2 Display"] = {sid: "dcmotorcontrol:11"};
	this.sidHashMap["dcmotorcontrol:11"] = {rtwname: "<Root>/EN-2 Display"};
	this.rtwnameHashMap["<Root>/GND1"] = {sid: "dcmotorcontrol:6"};
	this.sidHashMap["dcmotorcontrol:6"] = {rtwname: "<Root>/GND1"};
	this.rtwnameHashMap["<Root>/GND2"] = {sid: "dcmotorcontrol:8"};
	this.sidHashMap["dcmotorcontrol:8"] = {rtwname: "<Root>/GND2"};
	this.rtwnameHashMap["<Root>/Mux"] = {sid: "dcmotorcontrol:13"};
	this.sidHashMap["dcmotorcontrol:13"] = {rtwname: "<Root>/Mux"};
	this.rtwnameHashMap["<Root>/Scope"] = {sid: "dcmotorcontrol:12"};
	this.sidHashMap["dcmotorcontrol:12"] = {rtwname: "<Root>/Scope"};
	this.rtwnameHashMap["<Root>/VCC1"] = {sid: "dcmotorcontrol:4"};
	this.sidHashMap["dcmotorcontrol:4"] = {rtwname: "<Root>/VCC1"};
	this.rtwnameHashMap["<Root>/VCC2"] = {sid: "dcmotorcontrol:7"};
	this.sidHashMap["dcmotorcontrol:7"] = {rtwname: "<Root>/VCC2"};
	this.rtwnameHashMap["<S1>/In1"] = {sid: "dcmotorcontrol:2:116"};
	this.sidHashMap["dcmotorcontrol:2:116"] = {rtwname: "<S1>/In1"};
	this.rtwnameHashMap["<S1>/Data Type Conversion"] = {sid: "dcmotorcontrol:2:114"};
	this.sidHashMap["dcmotorcontrol:2:114"] = {rtwname: "<S1>/Data Type Conversion"};
	this.rtwnameHashMap["<S1>/Digital Output"] = {sid: "dcmotorcontrol:2:214"};
	this.sidHashMap["dcmotorcontrol:2:214"] = {rtwname: "<S1>/Digital Output"};
	this.rtwnameHashMap["<S2>/In1"] = {sid: "dcmotorcontrol:3:116"};
	this.sidHashMap["dcmotorcontrol:3:116"] = {rtwname: "<S2>/In1"};
	this.rtwnameHashMap["<S2>/Data Type Conversion"] = {sid: "dcmotorcontrol:3:114"};
	this.sidHashMap["dcmotorcontrol:3:114"] = {rtwname: "<S2>/Data Type Conversion"};
	this.rtwnameHashMap["<S2>/Digital Output"] = {sid: "dcmotorcontrol:3:214"};
	this.sidHashMap["dcmotorcontrol:3:214"] = {rtwname: "<S2>/Digital Output"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
