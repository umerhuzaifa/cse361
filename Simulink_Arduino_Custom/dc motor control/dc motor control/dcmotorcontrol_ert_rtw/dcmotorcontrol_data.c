/*
 * File: dcmotorcontrol_data.c
 *
 * Code generated for Simulink model 'dcmotorcontrol'.
 *
 * Model version                  : 1.9
 * Simulink Coder version         : 8.10 (R2016a) 10-Feb-2016
 * C/C++ source code generated on : Thu Sep 14 22:31:02 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "dcmotorcontrol.h"
#include "dcmotorcontrol_private.h"

/* Block parameters (auto storage) */
P_dcmotorcontrol_T dcmotorcontrol_P = {
  9U,                                  /* Mask Parameter: DigitalOutput_pinNumber
                                        * Referenced by: '<S1>/Digital Output'
                                        */
  10U,                                 /* Mask Parameter: DigitalOutput_pinNum_be11dsugft
                                        * Referenced by: '<S2>/Digital Output'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/GND2'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/VCC2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/GND1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/VCC1'
                                        */
  0U,                                  /* Computed Parameter: EN1_CurrentSetting
                                        * Referenced by: '<Root>/EN-1'
                                        */
  1U                                   /* Computed Parameter: EN2_CurrentSetting
                                        * Referenced by: '<Root>/EN-2'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
