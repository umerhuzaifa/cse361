/*
 * File: dcmotorcontrol.c
 *
 * Code generated for Simulink model 'dcmotorcontrol'.
 *
 * Model version                  : 1.9
 * Simulink Coder version         : 8.10 (R2016a) 10-Feb-2016
 * C/C++ source code generated on : Thu Sep 14 22:31:02 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "dcmotorcontrol.h"
#include "dcmotorcontrol_private.h"
#include "dcmotorcontrol_dt.h"

/* Block signals (auto storage) */
B_dcmotorcontrol_T dcmotorcontrol_B;

/* Block states (auto storage) */
DW_dcmotorcontrol_T dcmotorcontrol_DW;

/* Real-time model */
RT_MODEL_dcmotorcontrol_T dcmotorcontrol_M_;
RT_MODEL_dcmotorcontrol_T *const dcmotorcontrol_M = &dcmotorcontrol_M_;

/* Model step function */
void dcmotorcontrol_step(void)
{
  uint8_T tmp;

  /* ManualSwitch: '<Root>/EN-1' incorporates:
   *  Constant: '<Root>/GND1'
   *  Constant: '<Root>/VCC1'
   */
  if (dcmotorcontrol_P.EN1_CurrentSetting == 1) {
    dcmotorcontrol_B.EN1 = dcmotorcontrol_P.VCC1_Value;
  } else {
    dcmotorcontrol_B.EN1 = dcmotorcontrol_P.GND1_Value;
  }

  /* End of ManualSwitch: '<Root>/EN-1' */

  /* DataTypeConversion: '<S1>/Data Type Conversion' */
  if (dcmotorcontrol_B.EN1 < 256.0) {
    if (dcmotorcontrol_B.EN1 >= 0.0) {
      tmp = (uint8_T)dcmotorcontrol_B.EN1;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint8_T;
  }

  /* End of DataTypeConversion: '<S1>/Data Type Conversion' */

  /* S-Function (arduinodigitaloutput_sfcn): '<S1>/Digital Output' */
  MW_digitalWrite(dcmotorcontrol_P.DigitalOutput_pinNumber, tmp);

  /* ManualSwitch: '<Root>/EN-2' incorporates:
   *  Constant: '<Root>/GND2'
   *  Constant: '<Root>/VCC2'
   */
  if (dcmotorcontrol_P.EN2_CurrentSetting == 1) {
    dcmotorcontrol_B.EN2 = dcmotorcontrol_P.VCC2_Value;
  } else {
    dcmotorcontrol_B.EN2 = dcmotorcontrol_P.GND2_Value;
  }

  /* End of ManualSwitch: '<Root>/EN-2' */

  /* DataTypeConversion: '<S2>/Data Type Conversion' */
  if (dcmotorcontrol_B.EN2 < 256.0) {
    if (dcmotorcontrol_B.EN2 >= 0.0) {
      tmp = (uint8_T)dcmotorcontrol_B.EN2;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint8_T;
  }

  /* End of DataTypeConversion: '<S2>/Data Type Conversion' */

  /* S-Function (arduinodigitaloutput_sfcn): '<S2>/Digital Output' */
  MW_digitalWrite(dcmotorcontrol_P.DigitalOutput_pinNum_be11dsugft, tmp);

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.02s, 0.0s] */
    rtExtModeUpload(0, dcmotorcontrol_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.02s, 0.0s] */
    if ((rtmGetTFinal(dcmotorcontrol_M)!=-1) &&
        !((rtmGetTFinal(dcmotorcontrol_M)-dcmotorcontrol_M->Timing.taskTime0) >
          dcmotorcontrol_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(dcmotorcontrol_M, "Simulation finished");
    }

    if (rtmGetStopRequested(dcmotorcontrol_M)) {
      rtmSetErrorStatus(dcmotorcontrol_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++dcmotorcontrol_M->Timing.clockTick0)) {
    ++dcmotorcontrol_M->Timing.clockTickH0;
  }

  dcmotorcontrol_M->Timing.taskTime0 = dcmotorcontrol_M->Timing.clockTick0 *
    dcmotorcontrol_M->Timing.stepSize0 + dcmotorcontrol_M->Timing.clockTickH0 *
    dcmotorcontrol_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void dcmotorcontrol_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)dcmotorcontrol_M, 0,
                sizeof(RT_MODEL_dcmotorcontrol_T));
  rtmSetTFinal(dcmotorcontrol_M, -1);
  dcmotorcontrol_M->Timing.stepSize0 = 0.02;

  /* External mode info */
  dcmotorcontrol_M->Sizes.checksums[0] = (2698999623U);
  dcmotorcontrol_M->Sizes.checksums[1] = (4003503253U);
  dcmotorcontrol_M->Sizes.checksums[2] = (433350263U);
  dcmotorcontrol_M->Sizes.checksums[3] = (1706878125U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[5];
    dcmotorcontrol_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(dcmotorcontrol_M->extModeInfo,
      &dcmotorcontrol_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(dcmotorcontrol_M->extModeInfo,
                        dcmotorcontrol_M->Sizes.checksums);
    rteiSetTPtr(dcmotorcontrol_M->extModeInfo, rtmGetTPtr(dcmotorcontrol_M));
  }

  /* block I/O */
  (void) memset(((void *) &dcmotorcontrol_B), 0,
                sizeof(B_dcmotorcontrol_T));

  /* states (dwork) */
  (void) memset((void *)&dcmotorcontrol_DW, 0,
                sizeof(DW_dcmotorcontrol_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    dcmotorcontrol_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Start for S-Function (arduinodigitaloutput_sfcn): '<S1>/Digital Output' */
  MW_pinModeOutput(dcmotorcontrol_P.DigitalOutput_pinNumber);

  /* Start for S-Function (arduinodigitaloutput_sfcn): '<S2>/Digital Output' */
  MW_pinModeOutput(dcmotorcontrol_P.DigitalOutput_pinNum_be11dsugft);
}

/* Model terminate function */
void dcmotorcontrol_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
