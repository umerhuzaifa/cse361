/*
 * File: dcmotorcontrol_types.h
 *
 * Code generated for Simulink model 'dcmotorcontrol'.
 *
 * Model version                  : 1.9
 * Simulink Coder version         : 8.10 (R2016a) 10-Feb-2016
 * C/C++ source code generated on : Thu Sep 14 22:31:02 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_dcmotorcontrol_types_h_
#define RTW_HEADER_dcmotorcontrol_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_dcmotorcontrol_T_ P_dcmotorcontrol_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_dcmotorcontrol_T RT_MODEL_dcmotorcontrol_T;

#endif                                 /* RTW_HEADER_dcmotorcontrol_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
