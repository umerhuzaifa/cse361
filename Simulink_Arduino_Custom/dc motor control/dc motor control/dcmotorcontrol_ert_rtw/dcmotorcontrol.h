/*
 * File: dcmotorcontrol.h
 *
 * Code generated for Simulink model 'dcmotorcontrol'.
 *
 * Model version                  : 1.9
 * Simulink Coder version         : 8.10 (R2016a) 10-Feb-2016
 * C/C++ source code generated on : Thu Sep 14 22:31:02 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Atmel->AVR
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_dcmotorcontrol_h_
#define RTW_HEADER_dcmotorcontrol_h_
#include <float.h>
#include <string.h>
#include <stddef.h>
#ifndef dcmotorcontrol_COMMON_INCLUDES_
# define dcmotorcontrol_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "arduino_digitaloutput_lct.h"
#endif                                 /* dcmotorcontrol_COMMON_INCLUDES_ */

#include "dcmotorcontrol_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T EN1;                          /* '<Root>/EN-1' */
  real_T EN2;                          /* '<Root>/EN-2' */
} B_dcmotorcontrol_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  struct {
    void *LoggedData;
  } Scope_PWORK;                       /* '<Root>/Scope' */
} DW_dcmotorcontrol_T;

/* Parameters (auto storage) */
struct P_dcmotorcontrol_T_ {
  uint32_T DigitalOutput_pinNumber;    /* Mask Parameter: DigitalOutput_pinNumber
                                        * Referenced by: '<S1>/Digital Output'
                                        */
  uint32_T DigitalOutput_pinNum_be11dsugft;/* Mask Parameter: DigitalOutput_pinNum_be11dsugft
                                            * Referenced by: '<S2>/Digital Output'
                                            */
  real_T GND2_Value;                   /* Expression: 0
                                        * Referenced by: '<Root>/GND2'
                                        */
  real_T VCC2_Value;                   /* Expression: 1
                                        * Referenced by: '<Root>/VCC2'
                                        */
  real_T GND1_Value;                   /* Expression: 0
                                        * Referenced by: '<Root>/GND1'
                                        */
  real_T VCC1_Value;                   /* Expression: 1
                                        * Referenced by: '<Root>/VCC1'
                                        */
  uint8_T EN1_CurrentSetting;          /* Computed Parameter: EN1_CurrentSetting
                                        * Referenced by: '<Root>/EN-1'
                                        */
  uint8_T EN2_CurrentSetting;          /* Computed Parameter: EN2_CurrentSetting
                                        * Referenced by: '<Root>/EN-2'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_dcmotorcontrol_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_dcmotorcontrol_T dcmotorcontrol_P;

/* Block signals (auto storage) */
extern B_dcmotorcontrol_T dcmotorcontrol_B;

/* Block states (auto storage) */
extern DW_dcmotorcontrol_T dcmotorcontrol_DW;

/* Model entry point functions */
extern void dcmotorcontrol_initialize(void);
extern void dcmotorcontrol_step(void);
extern void dcmotorcontrol_terminate(void);

/* Real-time Model object */
extern RT_MODEL_dcmotorcontrol_T *const dcmotorcontrol_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'dcmotorcontrol'
 * '<S1>'   : 'dcmotorcontrol/EN-1 Digital Output'
 * '<S2>'   : 'dcmotorcontrol/EN-2 Digital Output1'
 */
#endif                                 /* RTW_HEADER_dcmotorcontrol_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
